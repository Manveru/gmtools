package controllers;//

import com.esotericsoftware.kryonet.KryoNetException;
import com.esotericsoftware.kryonet.util.TcpIdleSender;
import java.io.IOException;
import java.io.InputStream;

public abstract class InputStreamImageSender extends TcpIdleSender {
    private final InputStream input;
    private final byte[] chunk;
    private boolean stopping = false;

    public InputStreamImageSender(InputStream input, int chunkSize) {
        this.input = input;
        this.chunk = new byte[chunkSize];
    }

    protected final Object next() {
        int count;
        try {
            for(int ex = 0; ex < this.chunk.length; ex += count) {
                count = this.input.read(this.chunk, ex, this.chunk.length - ex);
                if(count < 0) {
                    if(ex == 0) {
                        if(stopping)
                        {
                            return null;
                        }
                        else {
                            stopping = true;
                            return this.next(null);
                        }
                    }

                    byte[] partial = new byte[ex];
                    System.arraycopy(this.chunk, 0, partial, 0, ex);
                    return this.next(partial);
                }
            }
        } catch (IOException var4) {
            throw new KryoNetException(var4);
        }

        return this.next(this.chunk);
    }

    protected abstract Object next(byte[] var1);
}
