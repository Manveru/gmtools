package ui;

import controllers.Controller;
import controllers.ImageListener;
import javafx.application.Platform;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Path;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

/**
 * Created by Michel on 22-3-2015.
 */
public class MainWindow extends StackPane implements ImageListener {
    private Controller controller;
    private Pane pane;
    private Shape fow;
    private ImageView imageview;
    public MainWindow(String ipadress) {

// Use a pane pane as the root for scene
        pane = new Pane();
        Platform.setImplicitExit(false);
        controller = new Controller(ipadress);
        controller.addListener(this);
        controller.StartClient();
        getChildren().addAll(pane);
        //setStyle("-fx-background-color: black;");
        //setMouseTransparent(true);

    }

    @Override
    public void newImageSet(ImageSelection image) {
        Platform.runLater(() -> {
            pane.getChildren().retainAll();
            System.out.println(image.getImage().getWidth());
            fow = new Rectangle(image.getImage().getWidth(), image.getImage().getHeight());
            fow.setFill(Color.rgb(0, 0, 0));

            for (Polygon rect : image.getRectangleArrayList()) {
                fow = Path.subtract(fow, rect);
            }
            fow.setStrokeWidth(1);
            fow.setStroke(Color.BLACK);
            imageview = new ImageView(image.getImage());
            imageview.setX(0);
            imageview.setY(0);

            pane.getChildren().add(imageview);
            pane.getChildren().add(fow);
        });

    }

    @Override
    public void translate(double x, double y) {
        Platform.runLater(() -> {
            System.out.println(x + " " + y);
            pane.setTranslateX(x);
            pane.setTranslateY(y);
        });
    }


    @Override
    public void zoom(double x, double y) {
        Platform.runLater(() -> {
            pane.setScaleX(x);
            pane.setScaleY(y);
        });
    }

    @Override
    public void subtract(Polygon rectangle) {
        Platform.runLater(() -> {
            if (imageview != null) {
                System.out.println("New rect");
                pane.getChildren().clear();
                fow = Path.subtract(fow, rectangle);
                pane.getChildren().addAll(imageview, fow);
            }
        });
    }
}
