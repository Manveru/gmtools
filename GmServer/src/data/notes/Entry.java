package data.notes;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Michel on 31-3-2015.
 */
public class Entry implements Serializable {
    private String titel;
    private ArrayList<Item> items = new ArrayList<>();
    private ArrayList<Relation> relations = new ArrayList<>();
    private ArrayList<Entry> subEntries = new ArrayList<>();
    private NoteTypeEnum type;

    public Entry() {
        subEntries = new ArrayList<>();
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public NoteTypeEnum getType() {
        return type;
    }

    public void setType(NoteTypeEnum type) {
        this.type = type;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public ArrayList<Relation> getRelations() {
        return relations;
    }

    public void setRelations(ArrayList<Relation> relations) {
        this.relations = relations;
    }

    public ArrayList<Entry> getSubEntries() {
        return subEntries;
    }

    public void setSubEntries(ArrayList<Entry> subEntries) {
        this.subEntries = subEntries;
    }

    public void addSubEntry(Entry entry) {
        subEntries.add(entry);
    }

    @Override
    public String toString() {
        return titel;
    }

    public void addItem(Item item) {
        items.add(item);
    }
}
