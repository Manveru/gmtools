package controllers;

import data.Feat;
import data.MagicItem;
import data.NPC;
import data.Spell;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Michel on 28-3-2015.
 */
public class CsvController {
    public ArrayList<NPC> parseNpcCsv()
    {
        ArrayList<NPC> NPCs = new ArrayList<NPC>();
        try {
            int i = 0;
            String path = NoteController.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
            CSVFormat format = CSVFormat.RFC4180.withHeader().withDelimiter(',');
            File csvData = new File(path, "data\\monster_stat_blocks_full.csv");
            CSVParser parser = CSVParser.parse(csvData, Charset.defaultCharset(), format);
            for (CSVRecord csvRecord : parser) {
                NPC NPC = new NPC();
                NPC.parseObject(csvRecord);
                NPCs.add(NPC);
            }
            System.out.println(NPCs.size());

            Collections.sort(NPCs, new Comparator<NPC>() {
                @Override
                public int compare(NPC fruite1, NPC fruite2) {

                    return fruite1.getName().compareTo(fruite2.getName());
                }
            });
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return NPCs;
    }

    public ArrayList<Feat> parseFeatsCsv()
    {
        ArrayList<Feat> feats = new ArrayList<Feat>();
        try {
            String path = NoteController.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();

            CSVFormat format = CSVFormat.RFC4180.withHeader().withDelimiter(',');
            File csvData = new File(path, "\\data\\feats.csv");
            CSVParser parser = CSVParser.parse(csvData, Charset.defaultCharset(), format);
            for (CSVRecord csvRecord : parser) {
                Feat feat = new Feat();
                feat.parseObject(csvRecord);
                feats.add(feat);
            }
            System.out.println(feats.size());

            Collections.sort(feats, new Comparator<Feat>() {
                @Override
                public int compare(Feat fruite1, Feat fruite2) {

                    return fruite1.getName().compareTo(fruite2.getName());
                }
            });
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return feats;
    }

    public ArrayList<Spell> parseSpellsCsv() {
        ArrayList<Spell> spells = new ArrayList<>();
        try {
            String path = NoteController.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();

            CSVFormat format = CSVFormat.RFC4180.withHeader().withDelimiter(',');
            File csvData = new File(path, "\\data\\spell_full.csv");
            CSVParser parser = CSVParser.parse(csvData, Charset.defaultCharset(), format);
            for (CSVRecord csvRecord : parser) {
                Spell spell = new Spell();
                spell.parseObject(csvRecord);
                spells.add(spell);
            }
            System.out.println(spells.size());

            Collections.sort(spells, new Comparator<Spell>() {
                @Override
                public int compare(Spell fruite1, Spell fruite2) {

                    return fruite1.getName().compareTo(fruite2.getName());
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return spells;
    }

    public ArrayList<MagicItem> parseMagicItemCsv()
    {
        ArrayList<MagicItem> items = new ArrayList<MagicItem>();
        try {
            String path = NoteController.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();

            CSVFormat format = CSVFormat.RFC4180.withHeader().withDelimiter(',');
            File csvData = new File(path, "\\data\\magic_items_full.csv");
            CSVParser parser = CSVParser.parse(csvData, Charset.defaultCharset(), format);
            for (CSVRecord csvRecord : parser) {
                MagicItem item = new MagicItem();
                item.parseObject(csvRecord);
                items.add(item);
            }
            System.out.println(items.size());

            Collections.sort(items, new Comparator<MagicItem>() {
                @Override
                public int compare(MagicItem fruite1, MagicItem fruite2) {

                    return fruite1.getName().compareTo(fruite2.getName());
                }
            });
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return items;
    }
}
