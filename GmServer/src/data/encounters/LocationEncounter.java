package data.encounters;

import java.util.HashMap;

/**
 * Created by Michel on 4-4-2015.
 */
public class LocationEncounter {
    HashMap<Integer, String> encounters = new HashMap<>();
    private String location;

    public void addEncounter(int key, String value) {
        encounters.put(key, value);
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public HashMap<Integer, String> getEncounters() {
        return encounters;
    }

    public void setEncounters(HashMap<Integer, String> encounters) {
        this.encounters = encounters;
    }

    public String getEncounter(int randValue) {

        System.out.println(randValue);
        if (encounters.containsKey(randValue)) {
            return encounters.get(randValue);
        }
        return null;
    }
}
