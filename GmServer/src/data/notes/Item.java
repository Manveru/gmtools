package data.notes;

import java.io.Serializable;

/**
 * Created by Michel on 31-3-2015.
 */
public class Item implements Serializable {
    private String content;
    private boolean known;
    private ItemTypeEnum type;


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isKnown() {
        return known;
    }

    public void setKnown(boolean known) {
        this.known = known;
    }

    public ItemTypeEnum getType() {
        return type;
    }

    public void setType(ItemTypeEnum type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return content;

    }
}
