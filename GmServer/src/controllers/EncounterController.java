package controllers;

import data.encounters.LocationEncounter;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Michel on 4-4-2015.
 */
public class EncounterController {
    public static ArrayList<LocationEncounter> getLocations() {
        ArrayList<LocationEncounter> encounters = new ArrayList<>();
        try {
            String path = NoteController.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();

            File goblinData = new File(path, "data\\encounters\\goblin_den.txt");
            Scanner scanner = new Scanner(goblinData);

            LocationEncounter encounter = new LocationEncounter();
            encounter.setLocation("Goblin den");
            while (scanner.hasNextLine()) {


                String line = scanner.nextLine();
                System.out.println(line);
                String[] split = line.split(" ");
                if (split.length >= 6) {
                    System.out.println(split[0]);
                    String[] splitNumbers = split[0].split("–");
                    if (splitNumbers.length == 2) {
                        int start = Integer.parseInt(splitNumbers[0]);
                        int stop = Integer.parseInt(splitNumbers[1]);
                        System.out.println(start + " " + stop);
                        for (int i = start; i <= stop; i++) {
                            encounter.addEncounter(i, line);
                        }


                    } else {
                        encounter.addEncounter(Integer.parseInt(split[0]), line);
                    }
                }

            }

            encounters.add(encounter);
        } catch (URISyntaxException | FileNotFoundException e) {
            e.printStackTrace();
        }


        return encounters;
    }

}
