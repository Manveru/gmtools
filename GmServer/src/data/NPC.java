package data;

import org.apache.commons.csv.CSVRecord;

/**
 * Created by Michel on 28-3-2015.
 */
public class NPC extends PathfinderObject {
    private String Name;
    private String CR;
    private String XP;
    private String Race;
    private String aClass;
    private String MonsterSource;
    private String Alignment;
    private String Size;
    private String Type;
    private String SubType;
    private String Init;
    private String Senses;
    private String Aura;
    private String AC;
    private String AC_Mods;
    private String HP;
    private String HD;
    private String HP_Mods;
    private String Saves;
    private String Fort;
    private String Ref;
    private String Will;
    private String Save_Mods;
    private String DefensiveAbilities;
    private String DR;
    private String Immune;
    private String Resist;
    private String SR;
    private String Weaknesses;
    private String Speed;
    private String Speed_Mod;
    private String Melee;
    private String Ranged;
    private String Space;
    private String Reach;
    private String SpecialAttacks;
    private String SpellLikeAbilities;
    private String SpellsKnown;
    private String SpellsPrepared;
    private String SpellDomains;
    private String AbilityScores;
    private String AbilityScore_Mods;
    private String BaseAtk;
    private String CMB;
    private String CMD;
    private String Feats;
    private String Skills;
    private String RacialMods;
    private String Languages;
    private String SQ;
    private String Environment;
    private String Organization;
    private String Treasure;
    private String Description_Visual;
    private String Group;
    private String Source;
    private String IsTemplate;
    private String SpecialAbilities;
    private String Description;
    private String FullText;
    private String Gender;
    private String Bloodline;
    private String ProhibitedSchools;
    private String BeforeCombat;
    private String DuringCombat;
    private String Morale;
    private String Gear;
    private String OtherGear;
    private String Vulnerability;
    private String Note;
    private String CharacterFlag;
    private String CompanionFlag;
    private String Fly;
    private String Climb;
    private String Burrow;
    private String Swim;
    private String Land;
    private String TemplatesApplied;
    private String OffenseNote;
    private String BaseStatistics;
    private String ExtractsPrepared;
    private String AgeCategory;
    private String Mystery;
    private String ClassArchetypes;
    private String Patron;
    private String CompanionFamiliarLink;
    private String FocusedSchool;
    private String Traits;
    private String AlternateNameForm;
    private String LinkText;
    private String UniqueMonster;
    private String ThassilonianSpecialization;
    private String Variant;
    private String MR;
    private String Mythic;
    private String MT;

    @Override
    public void parseObject(CSVRecord record) {
        setName(record.get("Name"));
        setCR(record.get("CR"));
        setXP(record.get("XP"));
        setRace(record.get("Race"));
        setClassChar(record.get("Class"));
        setMonsterSource(record.get("MonsterSource"));
        setAlignment(record.get("Alignment"));
        setSize(record.get("Size"));
        setType(record.get("Type"));
        setSubType(record.get("SubType"));
        setInit(record.get("Init"));
        setSenses(record.get("Senses"));
        setAura(record.get("Aura"));
        setAC(record.get("AC"));
        setAC_Mods(record.get("AC_Mods"));
        setHP(record.get("HP"));
        setHD(record.get("HD"));
        setHP_Mods(record.get("HP_Mods"));
        setSaves(record.get("Saves"));
        setFort(record.get("Fort"));
        setRef(record.get("Ref"));
        setWill(record.get("Will"));
        setSave_Mods(record.get("Save_Mods"));
        setDefensiveAbilities(record.get("DefensiveAbilities"));
        setDR(record.get("DR"));
        setImmune(record.get("Immune"));
        setResist(record.get("Resist"));
        setSR(record.get("SR"));
        setWeaknesses(record.get("Weaknesses"));
        setSpeed(record.get("Speed"));
        setSpeed_Mod(record.get("Speed_Mod"));
        setMelee(record.get("Melee"));
        setRanged(record.get("Ranged"));
        setSpace(record.get("Space"));
        setReach(record.get("Reach"));
        setSpecialAttacks(record.get("SpecialAttacks"));
        setSpellLikeAbilities(record.get("SpellLikeAbilities"));
        setSpellsKnown(record.get("SpellsKnown"));
        setSpellsPrepared(record.get("SpellsPrepared"));
        setSpellDomains(record.get("SpellDomains"));
        setAbilityScores(record.get("AbilityScores"));
        setAbilityScore_Mods(record.get("AbilityScore_Mods"));
        setBaseAtk(record.get("BaseAtk"));
        setCMB(record.get("CMB"));
        setCMD(record.get("CMD"));
        setFeats(record.get("Feats"));
        setSkills(record.get("Skills"));
        setRacialMods(record.get("RacialMods"));
        setLanguages(record.get("Languages"));
        setSQ(record.get("SQ"));
        setEnvironment(record.get("Environment"));
        setOrganization(record.get("Organization"));
        setTreasure(record.get("Treasure"));
        setDescription_Visual(record.get("Description_Visual"));
        setGroup(record.get("Group"));
        setSource(record.get("Source"));
        setIsTemplate(record.get("IsTemplate"));
        setSpecialAbilities(record.get("SpecialAbilities"));
        setDescription(record.get("Description"));
        setFullText(record.get("FullText"));
        setGender(record.get("Gender"));
        setBloodline(record.get("Bloodline"));
        setProhibitedSchools(record.get("ProhibitedSchools"));
        setBeforeCombat(record.get("BeforeCombat"));
        setDuringCombat(record.get("DuringCombat"));
        setMorale(record.get("Morale"));
        setGear(record.get("Gear"));
        setOtherGear(record.get("OtherGear"));
        setVulnerability(record.get("Vulnerability"));
        setNote(record.get("Note"));
        setCharacterFlag(record.get("CharacterFlag"));
        setCompanionFlag(record.get("CompanionFlag"));
        setFly(record.get("Fly"));
        setClimb(record.get("Climb"));
        setBurrow(record.get("Burrow"));
        setSwim(record.get("Swim"));
        setLand(record.get("Land"));
        setTemplatesApplied(record.get("TemplatesApplied"));
        setOffenseNote(record.get("OffenseNote"));
        setBaseStatistics(record.get("BaseStatistics"));
        setExtractsPrepared(record.get("ExtractsPrepared"));
        setAgeCategory(record.get("AgeCategory"));
        setMystery(record.get("Mystery"));
        setClassArchetypes(record.get("ClassArchetypes"));
        setPatron(record.get("Patron"));
        setCompanionFamiliarLink(record.get("CompanionFamiliarLink"));
        setFocusedSchool(record.get("FocusedSchool"));
        setTraits(record.get("Traits"));
        setAlternateNameForm(record.get("AlternateNameForm"));
        setLinkText(record.get("LinkText"));
        setId(Integer.parseInt(record.get("id")));
        setUniqueMonster(record.get("UniqueMonster"));
        setThassilonianSpecialization(record.get("ThassilonianSpecialization"));
        setVariant(record.get("Variant"));
        setMR(record.get("MR"));
        setMythic(record.get("Mythic"));
        setMT(record.get("MT"));

    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCR() {
        return CR;
    }

    public void setCR(String CR) {
        this.CR = CR;
    }

    public String getXP() {
        return XP;
    }

    public void setXP(String XP) {
        this.XP = XP;
    }

    public String getRace() {
        return Race;
    }

    public void setRace(String race) {
        Race = race;
    }

    public String getClassChar() {
        return this.aClass;
    }

    public void setClassChar(String aClass) {
        this.aClass = aClass;
    }

    public String getMonsterSource() {
        return MonsterSource;
    }

    public void setMonsterSource(String monsterSource) {
        MonsterSource = monsterSource;
    }

    public String getAlignment() {
        return Alignment;
    }

    public void setAlignment(String alignment) {
        Alignment = alignment;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getSubType() {
        return SubType;
    }

    public void setSubType(String subType) {
        SubType = subType;
    }

    public String getInit() {
        return Init;
    }

    public void setInit(String init) {
        Init = init;
    }

    public String getSenses() {
        return Senses;
    }

    public void setSenses(String senses) {
        Senses = senses;
    }

    public String getAura() {
        return Aura;
    }

    public void setAura(String aura) {
        Aura = aura;
    }

    public String getAC() {
        return AC;
    }

    public void setAC(String AC) {
        this.AC = AC;
    }

    public String getAC_Mods() {
        return AC_Mods;
    }

    public void setAC_Mods(String AC_Mods) {
        this.AC_Mods = AC_Mods;
    }

    public String getHP() {
        return HP;
    }

    public void setHP(String HP) {
        this.HP = HP;
    }

    public String getHD() {
        return HD;
    }

    public void setHD(String HD) {
        this.HD = HD;
    }

    public String getHP_Mods() {
        return HP_Mods;
    }

    public void setHP_Mods(String HP_Mods) {
        this.HP_Mods = HP_Mods;
    }

    public String getSaves() {
        return Saves;
    }

    public void setSaves(String saves) {
        Saves = saves;
    }

    public String getFort() {
        return Fort;
    }

    public void setFort(String fort) {
        Fort = fort;
    }

    public String getRef() {
        return Ref;
    }

    public void setRef(String ref) {
        Ref = ref;
    }

    public String getWill() {
        return Will;
    }

    public void setWill(String will) {
        Will = will;
    }

    public String getSave_Mods() {
        return Save_Mods;
    }

    public void setSave_Mods(String save_Mods) {
        Save_Mods = save_Mods;
    }

    public String getDefensiveAbilities() {
        return DefensiveAbilities;
    }

    public void setDefensiveAbilities(String defensiveAbilities) {
        DefensiveAbilities = defensiveAbilities;
    }

    public String getDR() {
        return DR;
    }

    public void setDR(String DR) {
        this.DR = DR;
    }

    public String getImmune() {
        return Immune;
    }

    public void setImmune(String immune) {
        Immune = immune;
    }

    public String getResist() {
        return Resist;
    }

    public void setResist(String resist) {
        Resist = resist;
    }

    public String getSR() {
        return SR;
    }

    public void setSR(String SR) {
        this.SR = SR;
    }

    public String getWeaknesses() {
        return Weaknesses;
    }

    public void setWeaknesses(String weaknesses) {
        Weaknesses = weaknesses;
    }

    public String getSpeed() {
        return Speed;
    }

    public void setSpeed(String speed) {
        Speed = speed;
    }

    public String getSpeed_Mod() {
        return Speed_Mod;
    }

    public void setSpeed_Mod(String speed_Mod) {
        Speed_Mod = speed_Mod;
    }

    public String getMelee() {
        return Melee;
    }

    public void setMelee(String melee) {
        Melee = melee;
    }

    public String getRanged() {
        return Ranged;
    }

    public void setRanged(String ranged) {
        Ranged = ranged;
    }

    public String getSpace() {
        return Space;
    }

    public void setSpace(String space) {
        Space = space;
    }

    public String getReach() {
        return Reach;
    }

    public void setReach(String reach) {
        Reach = reach;
    }

    public String getSpecialAttacks() {
        return SpecialAttacks;
    }

    public void setSpecialAttacks(String specialAttacks) {
        SpecialAttacks = specialAttacks;
    }

    public String getSpellLikeAbilities() {
        return SpellLikeAbilities;
    }

    public void setSpellLikeAbilities(String spellLikeAbilities) {
        SpellLikeAbilities = spellLikeAbilities;
    }

    public String getSpellsKnown() {
        return SpellsKnown;
    }

    public void setSpellsKnown(String spellsKnown) {
        SpellsKnown = spellsKnown;
    }

    public String getSpellsPrepared() {
        return SpellsPrepared;
    }

    public void setSpellsPrepared(String spellsPrepared) {
        SpellsPrepared = spellsPrepared;
    }

    public String getSpellDomains() {
        return SpellDomains;
    }

    public void setSpellDomains(String spellDomains) {
        SpellDomains = spellDomains;
    }

    public String getAbilityScores() {
        return AbilityScores;
    }

    public void setAbilityScores(String abilityScores) {
        AbilityScores = abilityScores;
    }

    public String getAbilityScore_Mods() {
        return AbilityScore_Mods;
    }

    public void setAbilityScore_Mods(String abilityScore_Mods) {
        AbilityScore_Mods = abilityScore_Mods;
    }

    public String getBaseAtk() {
        return BaseAtk;
    }

    public void setBaseAtk(String baseAtk) {
        BaseAtk = baseAtk;
    }

    public String getCMB() {
        return CMB;
    }

    public void setCMB(String CMB) {
        this.CMB = CMB;
    }

    public String getCMD() {
        return CMD;
    }

    public void setCMD(String CMD) {
        this.CMD = CMD;
    }

    public String getFeats() {
        return Feats;
    }

    public void setFeats(String feats) {
        Feats = feats;
    }

    public String getSkills() {
        return Skills;
    }

    public void setSkills(String skills) {
        Skills = skills;
    }

    public String getRacialMods() {
        return RacialMods;
    }

    public void setRacialMods(String racialMods) {
        RacialMods = racialMods;
    }

    public String getLanguages() {
        return Languages;
    }

    public void setLanguages(String languages) {
        Languages = languages;
    }

    public String getSQ() {
        return SQ;
    }

    public void setSQ(String SQ) {
        this.SQ = SQ;
    }

    public String getEnvironment() {
        return Environment;
    }

    public void setEnvironment(String environment) {
        Environment = environment;
    }

    public String getOrganization() {
        return Organization;
    }

    public void setOrganization(String organization) {
        Organization = organization;
    }

    public String getTreasure() {
        return Treasure;
    }

    public void setTreasure(String treasure) {
        Treasure = treasure;
    }

    public String getDescription_Visual() {
        return Description_Visual;
    }

    public void setDescription_Visual(String description_Visual) {
        Description_Visual = description_Visual;
    }

    public String getGroup() {
        return Group;
    }

    public void setGroup(String group) {
        Group = group;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }

    public String getIsTemplate() {
        return IsTemplate;
    }

    public void setIsTemplate(String isTemplate) {
        IsTemplate = isTemplate;
    }

    public String getSpecialAbilities() {
        return SpecialAbilities;
    }

    public void setSpecialAbilities(String specialAbilities) {
        SpecialAbilities = specialAbilities;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getBloodline() {
        return Bloodline;
    }

    public void setBloodline(String bloodline) {
        Bloodline = bloodline;
    }

    public String getProhibitedSchools() {
        return ProhibitedSchools;
    }

    public void setProhibitedSchools(String prohibitedSchools) {
        ProhibitedSchools = prohibitedSchools;
    }

    public String getBeforeCombat() {
        return BeforeCombat;
    }

    public void setBeforeCombat(String beforeCombat) {
        BeforeCombat = beforeCombat;
    }

    public String getDuringCombat() {
        return DuringCombat;
    }

    public void setDuringCombat(String duringCombat) {
        DuringCombat = duringCombat;
    }

    public String getMorale() {
        return Morale;
    }

    public void setMorale(String morale) {
        Morale = morale;
    }

    public String getGear() {
        return Gear;
    }

    public void setGear(String gear) {
        Gear = gear;
    }

    public String getOtherGear() {
        return OtherGear;
    }

    public void setOtherGear(String otherGear) {
        OtherGear = otherGear;
    }

    public String getVulnerability() {
        return Vulnerability;
    }

    public void setVulnerability(String vulnerability) {
        Vulnerability = vulnerability;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getCharacterFlag() {
        return CharacterFlag;
    }

    public void setCharacterFlag(String characterFlag) {
        CharacterFlag = characterFlag;
    }

    public String getCompanionFlag() {
        return CompanionFlag;
    }

    public void setCompanionFlag(String companionFlag) {
        CompanionFlag = companionFlag;
    }

    public String getFly() {
        return Fly;
    }

    public void setFly(String fly) {
        Fly = fly;
    }

    public String getClimb() {
        return Climb;
    }

    public void setClimb(String climb) {
        Climb = climb;
    }

    public String getBurrow() {
        return Burrow;
    }

    public void setBurrow(String burrow) {
        Burrow = burrow;
    }

    public String getSwim() {
        return Swim;
    }

    public void setSwim(String swim) {
        Swim = swim;
    }

    public String getLand() {
        return Land;
    }

    public void setLand(String land) {
        Land = land;
    }

    public String getTemplatesApplied() {
        return TemplatesApplied;
    }

    public void setTemplatesApplied(String templatesApplied) {
        TemplatesApplied = templatesApplied;
    }

    public String getOffenseNote() {
        return OffenseNote;
    }

    public void setOffenseNote(String offenseNote) {
        OffenseNote = offenseNote;
    }

    public String getBaseStatistics() {
        return BaseStatistics;
    }

    public void setBaseStatistics(String baseStatistics) {
        BaseStatistics = baseStatistics;
    }

    public String getExtractsPrepared() {
        return ExtractsPrepared;
    }

    public void setExtractsPrepared(String extractsPrepared) {
        ExtractsPrepared = extractsPrepared;
    }

    public String getAgeCategory() {
        return AgeCategory;
    }

    public void setAgeCategory(String ageCategory) {
        AgeCategory = ageCategory;
    }

    public String getMystery() {
        return Mystery;
    }

    public void setMystery(String mystery) {
        Mystery = mystery;
    }

    public String getClassArchetypes() {
        return ClassArchetypes;
    }

    public void setClassArchetypes(String classArchetypes) {
        ClassArchetypes = classArchetypes;
    }

    public String getPatron() {
        return Patron;
    }

    public void setPatron(String patron) {
        Patron = patron;
    }

    public String getCompanionFamiliarLink() {
        return CompanionFamiliarLink;
    }

    public void setCompanionFamiliarLink(String companionFamiliarLink) {
        CompanionFamiliarLink = companionFamiliarLink;
    }

    public String getFocusedSchool() {
        return FocusedSchool;
    }

    public void setFocusedSchool(String focusedSchool) {
        FocusedSchool = focusedSchool;
    }

    public String getTraits() {
        return Traits;
    }

    public void setTraits(String traits) {
        Traits = traits;
    }

    public String getAlternateNameForm() {
        return AlternateNameForm;
    }

    public void setAlternateNameForm(String alternateNameForm) {
        AlternateNameForm = alternateNameForm;
    }

    public String getLinkText() {
        return LinkText;
    }

    public void setLinkText(String linkText) {
        LinkText = linkText;
    }

    public String getUniqueMonster() {
        return UniqueMonster;
    }

    public void setUniqueMonster(String uniqueMonster) {
        UniqueMonster = uniqueMonster;
    }

    public String getThassilonianSpecialization() {
        return ThassilonianSpecialization;
    }

    public void setThassilonianSpecialization(String thassilonianSpecialization) {
        ThassilonianSpecialization = thassilonianSpecialization;
    }

    public String getVariant() {
        return Variant;
    }

    public void setVariant(String variant) {
        Variant = variant;
    }

    public String getMR() {
        return MR;
    }

    public void setMR(String MR) {
        this.MR = MR;
    }

    public String getMythic() {
        return Mythic;
    }

    public void setMythic(String mythic) {
        Mythic = mythic;
    }

    public String getMT() {
        return MT;
    }

    public void setMT(String MT) {
        this.MT = MT;
    }

}
