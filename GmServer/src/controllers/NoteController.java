package controllers;

import data.notes.Project;

import java.io.File;
import java.net.URISyntaxException;

/**
 * Created by Michel on 1-4-2015.
 */
public class NoteController {
    private String noteDirectory = "projects";

    public Project newProject(String name) {
        try {
            String path = NoteController.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();

            if (directoryExists(path, noteDirectory)) {
                if (directoryExists(path + "\\" + noteDirectory, name)) {
                    return new Project(name);
                }
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }


    public boolean directoryExists(String path, String name) {

        File theDir = new File(path + "\\" + name);
        System.out.println(path + "\\" + name);
        if (!theDir.exists()) {
            System.out.println("creating directory: " + name);
            boolean result = false;

            try {
                theDir.mkdir();
                result = true;
            } catch (SecurityException se) {
                se.printStackTrace();
            }
            if (result) {
                System.out.println("DIR created");
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    public void SaveProject(Project currentProject) {

        try {
            String path = NoteController.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
            if (directoryExists(path, noteDirectory)) {
                if (directoryExists(path + "\\" + noteDirectory, currentProject.getTitel())) {
                    Serializer.SaveProject(path + "\\" + noteDirectory + "\\" + currentProject.getTitel(), currentProject.getTitel(), currentProject);
                }
            }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
