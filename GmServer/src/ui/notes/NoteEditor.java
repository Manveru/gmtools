package ui.notes;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.HTMLEditor;

/**
 * Created by Michel on 4-4-2015.
 */
public class NoteEditor extends HTMLEditor {
    final Delta dragDelta = new Delta();

    public NoteEditor() {

        setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                // record a delta distance for the drag and drop operation.
                dragDelta.x = getLayoutX() - mouseEvent.getSceneX();
                dragDelta.y = getLayoutY() - mouseEvent.getSceneY();
                setCursor(Cursor.MOVE);
            }
        });
        setOnMouseReleased(mouseEvent -> {
            setCursor(Cursor.HAND);
        });
        setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                setLayoutX(mouseEvent.getSceneX() + dragDelta.x);
                setLayoutY(mouseEvent.getSceneY() + dragDelta.y);
            }
        });
        setOnMouseEntered(mouseEvent -> {
            setCursor(Cursor.HAND);
        });
    }
}

// records relative x and y co-ordinates.
class Delta {
    double x, y;
}
