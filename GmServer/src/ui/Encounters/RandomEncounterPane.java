package ui.Encounters;

import controllers.EncounterController;
import data.encounters.LocationEncounter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Callback;
import ui.HtmlGenerator;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Michel on 29-3-2015.
 */
public class RandomEncounterPane extends BorderPane {

    final WebView browser = new WebView();
    final WebEngine webEngine = browser.getEngine();
    public ArrayList<LocationEncounter> locations = new ArrayList<>();
    public LocationEncounter currentLocationEncounter = null;

    public RandomEncounterPane() {
        ListView<LocationEncounter> listView = new ListView<>();


        //monsterText.setDisable(true);
        locations = EncounterController.getLocations();

        setCenter(browser);
        ObservableList<LocationEncounter> myObservableList = FXCollections.observableList(locations);
        listView.setItems(myObservableList);

        listView.setCellFactory(new Callback<ListView<LocationEncounter>, ListCell<LocationEncounter>>() {

            @Override
            public ListCell<LocationEncounter> call(ListView<LocationEncounter> p) {

                ListCell<LocationEncounter> cell = new ListCell<LocationEncounter>() {

                    @Override
                    protected void updateItem(LocationEncounter t, boolean bln) {
                        super.updateItem(t, bln);
                        if (t != null) {
                            setText(t.getLocation());
                        } else {
                            setText(null);
                        }
                    }

                };

                return cell;
            }
        });

        Button randomButton = new Button("Random");
        randomButton.setOnAction(event -> {
            generateEncounter();
        });

        VBox vbox = new VBox(10);
        VBox.setVgrow(listView, Priority.ALWAYS);
        vbox.getChildren().addAll(listView, randomButton);
        setLeft(vbox);


        listView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                currentLocationEncounter = newValue;
            }
        });


    }

    private void generateEncounter() {
        if (currentLocationEncounter != null) {
            Random random = new Random(System.currentTimeMillis());
            int randValue = random.nextInt(99) + 1;

            String encounter = currentLocationEncounter.getEncounter(randValue);
            webEngine.loadContent(HtmlGenerator.getHtml(encounter));
        }
    }


}
