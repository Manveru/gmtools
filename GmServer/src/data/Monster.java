package data;

import org.apache.commons.csv.CSVRecord;

/**
 * Created by Michel on 29-3-2015.
 */
public class Monster extends PathfinderObject
{
    private String Name;
    private String CR;
    private String XP;
    private String Race;
    private String Class1;
    private String Class1_Lvl;
    private String Class2;
    private String Class2_Lvl;
    private String Alignment;
    private String Size;
    private String Type;
    private String subtype1;
    private String subtype2;
    private String subtype3;
    private String subtype4;
    private String subtype5;
    private String subtype6;
    private String AC;
    private String AC_Touch;
    private String AC_Flat_footed;
    private String HP;
    private String HD;
    private String Fort;
    private String Ref;
    private String Will;
    private String Melee;
    private String Ranged;
    private String Space;
    private String Reach;
    private String Str;
    private String Dex;
    private String Con;
    private String Int;
    private String Wis;
    private String Cha;
    private String Feats;
    private String Skills;
    private String RacialMods;
    private String Languages;
    private String SQ;
    private String Environment;
    private String Organization;
    private String Treasure;
    private String Group;
    private String Gear;
    private String OtherGear;
    private String CharacterFlag;
    private String CompanionFlag;
    private String Speed;
    private String Base_Speed;
    private String Fly_Speed;
    private String Maneuverability;
    private String Climb_Speed;
    private String Swim_Speed;
    private String Burrow_Speed;
    private String Speed_Special;
    private String Speed_Land;
    private String Fly;
    private String Climb;
    private String Burrow;
    private String Swim;
    private String VariantParent;
    private String ClassArchetypes;
    private String CompanionFamiliarLink;
    private String AlternateNameForm;
    private String UniqueMonster;
    private String MR;
    private String Mythic;
    private String MT;
    private String Source;

    @Override
    public void parseObject(CSVRecord record) {
        setName(record.get("Name"));
        setCR(record.get("CR"));
        setXP(record.get("XP"));
        setRace(record.get("Race"));
        setClass1(record.get("Class1"));
        setClass1_Lvl(record.get("Class1_Lvl"));
        setClass2(record.get("Class2"));
        setClass2_Lvl(record.get("Class2_Lvl"));
        setAlignment(record.get("Alignment"));
        setSize(record.get("Size"));
        setType(record.get("Type"));
        setSubtype1(record.get("subtype1"));
        setSubtype2(record.get("subtype2"));
        setSubtype3(record.get("subtype3"));
        setSubtype4(record.get("subtype4"));
        setSubtype5(record.get("subtype5"));
        setSubtype6(record.get("subtype6"));
        setAC(record.get("AC"));
        setAC_Touch(record.get("AC_Touch"));
        setAC_Flat_footed(record.get("AC_Flat-footed"));
        setHP(record.get("HP"));
        setHD(record.get("HD"));
        setFort(record.get("Fort"));
        setRef(record.get("Ref"));
        setWill(record.get("Will"));
        setMelee(record.get("Melee"));
        setRanged(record.get("Ranged"));
        setSpace(record.get("Space"));
        setReach(record.get("Reach"));
        setStr(record.get("Str"));
        setDex(record.get("Dex"));
        setCon(record.get("Con"));
        setInt(record.get("Int"));
        setWis(record.get("Wis"));
        setCha(record.get("Cha"));
        setFeats(record.get("Feats"));
        setSkills(record.get("Skills"));
        setRacialMods(record.get("RacialMods"));
        setLanguages(record.get("Languages"));
        setSQ(record.get("SQ"));
        setEnvironment(record.get("Environment"));
        setOrganization(record.get("Organization"));
        setTreasure(record.get("Treasure"));
        setGroup(record.get("Group"));
        setGear(record.get("Gear"));
        setOtherGear(record.get("OtherGear"));
        setCharacterFlag(record.get("CharacterFlag"));
        setCompanionFlag(record.get("CompanionFlag"));
        setSpeed(record.get("Speed"));
        setBase_Speed(record.get("Base_Speed"));
        setFly_Speed(record.get("Fly_Speed"));
        setManeuverability(record.get("Maneuverability"));
        setClimb_Speed(record.get("Climb_Speed"));
        setSwim_Speed(record.get("Swim_Speed"));
        setBurrow_Speed(record.get("Burrow_Speed"));
        setSpeed_Special(record.get("Speed_Special"));
        setSpeed_Land(record.get("Speed_Land"));
        setFly(record.get("Fly"));
        setClimb(record.get("Climb"));
        setBurrow(record.get("Burrow"));
        setSwim(record.get("Swim"));
        setVariantParent(record.get("VariantParent"));
        setClassArchetypes(record.get("ClassArchetypes"));
        setCompanionFamiliarLink(record.get("CompanionFamiliarLink"));
        setAlternateNameForm(record.get("AlternateNameForm"));
        setId(Integer.parseInt(record.get("id")));
        setUniqueMonster(record.get("UniqueMonster"));
        setMR(record.get("MR"));
        setMythic(record.get("Mythic"));
        setMT(record.get("MT"));
        setSource(record.get("Source"));

    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCR() {
        return CR;
    }

    public void setCR(String CR) {
        this.CR = CR;
    }

    public String getXP() {
        return XP;
    }

    public void setXP(String XP) {
        this.XP = XP;
    }

    public String getRace() {
        return Race;
    }

    public void setRace(String race) {
        Race = race;
    }

    public String getClass1() {
        return Class1;
    }

    public void setClass1(String class1) {
        Class1 = class1;
    }

    public String getClass1_Lvl() {
        return Class1_Lvl;
    }

    public void setClass1_Lvl(String class1_Lvl) {
        Class1_Lvl = class1_Lvl;
    }

    public String getClass2() {
        return Class2;
    }

    public void setClass2(String class2) {
        Class2 = class2;
    }

    public String getClass2_Lvl() {
        return Class2_Lvl;
    }

    public void setClass2_Lvl(String class2_Lvl) {
        Class2_Lvl = class2_Lvl;
    }

    public String getAlignment() {
        return Alignment;
    }

    public void setAlignment(String alignment) {
        Alignment = alignment;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getSubtype1() {
        return subtype1;
    }

    public void setSubtype1(String subtype1) {
        this.subtype1 = subtype1;
    }

    public String getSubtype2() {
        return subtype2;
    }

    public void setSubtype2(String subtype2) {
        this.subtype2 = subtype2;
    }

    public String getSubtype3() {
        return subtype3;
    }

    public void setSubtype3(String subtype3) {
        this.subtype3 = subtype3;
    }

    public String getSubtype4() {
        return subtype4;
    }

    public void setSubtype4(String subtype4) {
        this.subtype4 = subtype4;
    }

    public String getSubtype5() {
        return subtype5;
    }

    public void setSubtype5(String subtype5) {
        this.subtype5 = subtype5;
    }

    public String getSubtype6() {
        return subtype6;
    }

    public void setSubtype6(String subtype6) {
        this.subtype6 = subtype6;
    }

    public String getAC() {
        return AC;
    }

    public void setAC(String AC) {
        this.AC = AC;
    }

    public String getAC_Touch() {
        return AC_Touch;
    }

    public void setAC_Touch(String AC_Touch) {
        this.AC_Touch = AC_Touch;
    }

    public String getAC_Flat_footed() {
        return AC_Flat_footed;
    }

    public void setAC_Flat_footed(String AC_Flat_footed) {
        this.AC_Flat_footed = AC_Flat_footed;
    }

    public String getHP() {
        return HP;
    }

    public void setHP(String HP) {
        this.HP = HP;
    }

    public String getHD() {
        return HD;
    }

    public void setHD(String HD) {
        this.HD = HD;
    }

    public String getFort() {
        return Fort;
    }

    public void setFort(String fort) {
        Fort = fort;
    }

    public String getRef() {
        return Ref;
    }

    public void setRef(String ref) {
        Ref = ref;
    }

    public String getWill() {
        return Will;
    }

    public void setWill(String will) {
        Will = will;
    }

    public String getMelee() {
        return Melee;
    }

    public void setMelee(String melee) {
        Melee = melee;
    }

    public String getRanged() {
        return Ranged;
    }

    public void setRanged(String ranged) {
        Ranged = ranged;
    }

    public String getSpace() {
        return Space;
    }

    public void setSpace(String space) {
        Space = space;
    }

    public String getReach() {
        return Reach;
    }

    public void setReach(String reach) {
        Reach = reach;
    }

    public String getStr() {
        return Str;
    }

    public void setStr(String str) {
        Str = str;
    }

    public String getDex() {
        return Dex;
    }

    public void setDex(String dex) {
        Dex = dex;
    }

    public String getCon() {
        return Con;
    }

    public void setCon(String con) {
        Con = con;
    }

    public String getInt() {
        return Int;
    }

    public void setInt(String anInt) {
        Int = anInt;
    }

    public String getWis() {
        return Wis;
    }

    public void setWis(String wis) {
        Wis = wis;
    }

    public String getCha() {
        return Cha;
    }

    public void setCha(String cha) {
        Cha = cha;
    }

    public String getFeats() {
        return Feats;
    }

    public void setFeats(String feats) {
        Feats = feats;
    }

    public String getSkills() {
        return Skills;
    }

    public void setSkills(String skills) {
        Skills = skills;
    }

    public String getRacialMods() {
        return RacialMods;
    }

    public void setRacialMods(String racialMods) {
        RacialMods = racialMods;
    }

    public String getLanguages() {
        return Languages;
    }

    public void setLanguages(String languages) {
        Languages = languages;
    }

    public String getSQ() {
        return SQ;
    }

    public void setSQ(String SQ) {
        this.SQ = SQ;
    }

    public String getEnvironment() {
        return Environment;
    }

    public void setEnvironment(String environment) {
        Environment = environment;
    }

    public String getOrganization() {
        return Organization;
    }

    public void setOrganization(String organization) {
        Organization = organization;
    }

    public String getTreasure() {
        return Treasure;
    }

    public void setTreasure(String treasure) {
        Treasure = treasure;
    }

    public String getGroup() {
        return Group;
    }

    public void setGroup(String group) {
        Group = group;
    }

    public String getGear() {
        return Gear;
    }

    public void setGear(String gear) {
        Gear = gear;
    }

    public String getOtherGear() {
        return OtherGear;
    }

    public void setOtherGear(String otherGear) {
        OtherGear = otherGear;
    }

    public String getCharacterFlag() {
        return CharacterFlag;
    }

    public void setCharacterFlag(String characterFlag) {
        CharacterFlag = characterFlag;
    }

    public String getCompanionFlag() {
        return CompanionFlag;
    }

    public void setCompanionFlag(String companionFlag) {
        CompanionFlag = companionFlag;
    }

    public String getSpeed() {
        return Speed;
    }

    public void setSpeed(String speed) {
        Speed = speed;
    }

    public String getBase_Speed() {
        return Base_Speed;
    }

    public void setBase_Speed(String base_Speed) {
        Base_Speed = base_Speed;
    }

    public String getFly_Speed() {
        return Fly_Speed;
    }

    public void setFly_Speed(String fly_Speed) {
        Fly_Speed = fly_Speed;
    }

    public String getManeuverability() {
        return Maneuverability;
    }

    public void setManeuverability(String maneuverability) {
        Maneuverability = maneuverability;
    }

    public String getClimb_Speed() {
        return Climb_Speed;
    }

    public void setClimb_Speed(String climb_Speed) {
        Climb_Speed = climb_Speed;
    }

    public String getSwim_Speed() {
        return Swim_Speed;
    }

    public void setSwim_Speed(String swim_Speed) {
        Swim_Speed = swim_Speed;
    }

    public String getBurrow_Speed() {
        return Burrow_Speed;
    }

    public void setBurrow_Speed(String burrow_Speed) {
        Burrow_Speed = burrow_Speed;
    }

    public String getSpeed_Special() {
        return Speed_Special;
    }

    public void setSpeed_Special(String speed_Special) {
        Speed_Special = speed_Special;
    }

    public String getSpeed_Land() {
        return Speed_Land;
    }

    public void setSpeed_Land(String speed_Land) {
        Speed_Land = speed_Land;
    }

    public String getFly() {
        return Fly;
    }

    public void setFly(String fly) {
        Fly = fly;
    }

    public String getClimb() {
        return Climb;
    }

    public void setClimb(String climb) {
        Climb = climb;
    }

    public String getBurrow() {
        return Burrow;
    }

    public void setBurrow(String burrow) {
        Burrow = burrow;
    }

    public String getSwim() {
        return Swim;
    }

    public void setSwim(String swim) {
        Swim = swim;
    }

    public String getVariantParent() {
        return VariantParent;
    }

    public void setVariantParent(String variantParent) {
        VariantParent = variantParent;
    }

    public String getClassArchetypes() {
        return ClassArchetypes;
    }

    public void setClassArchetypes(String classArchetypes) {
        ClassArchetypes = classArchetypes;
    }

    public String getCompanionFamiliarLink() {
        return CompanionFamiliarLink;
    }

    public void setCompanionFamiliarLink(String companionFamiliarLink) {
        CompanionFamiliarLink = companionFamiliarLink;
    }

    public String getAlternateNameForm() {
        return AlternateNameForm;
    }

    public void setAlternateNameForm(String alternateNameForm) {
        AlternateNameForm = alternateNameForm;
    }

    public String getUniqueMonster() {
        return UniqueMonster;
    }

    public void setUniqueMonster(String uniqueMonster) {
        UniqueMonster = uniqueMonster;
    }

    public String getMR() {
        return MR;
    }

    public void setMR(String MR) {
        this.MR = MR;
    }

    public String getMythic() {
        return Mythic;
    }

    public void setMythic(String mythic) {
        Mythic = mythic;
    }

    public String getMT() {
        return MT;
    }

    public void setMT(String MT) {
        this.MT = MT;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }

}
