package ui;

import data.Feat;
import data.MagicItem;
import data.NPC;
import data.Spell;

/**
 * Created by Michel on 28-3-2015.
 */
public class HtmlGenerator
{
    public static String getNpcHtml(NPC NPC)
    {
        return "<style>\n" +
                "@import url(http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|PT+Serif:400,700,400italic,700italic);\n" +
                "\n" +
                "\t.wrapper {\n" +
                "\t\tposition: relative;\n" +
                "\t\toverflow: auto;\n" +
                "\t\tmargin: 5px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.scroller {\n" +
                "\t\tpadding: 10px;\n" +
                "\t\tmax-width: 1000px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.scroller table {\n" +
                "\t\twidth: 100%;\n" +
                "\t}\n" +
                "\t\n" +
                "/* Global */\n" +
                "body, img {\n" +
                "\tmargin: 0;\n" +
                "\tpadding: 0;\n" +
                "\tborder: 0;\n" +
                "}\n" +
                "\n" +
                "body {\n" +
                "\tfont-size: 14px;\n" +
                "\tfont-family: 'PT Serif', serif;\n" +
                "\tbackground: #fffdf8 url('/pathfinderRPG/prd/include/bg-new.png') top left no-repeat;\n" +
                "}\n" +
                "\n" +
                "p {\n" +
                "\tline-height: 1.4em;\n" +
                "\tmargin: 15px 0;\n" +
                "}\n" +
                "\n" +
                "h1 {\n" +
                "\tfont: normal 32px 'PT Serif', serif;\n" +
                "\tmargin: 30px 0 0 0;\n" +
                "}\n" +
                "\t\n" +
                "h2 {\n" +
                "\tfont: normal 26px 'PT Serif', serif;\n" +
                "\tmargin: 30px 0 0 0;\n" +
                "}\n" +
                "\n" +
                "h3 {\n" +
                "\tfont: normal 20px 'PT Serif', serif;\n" +
                "\tmargin: 30px 0 0 0;\n" +
                "}\n" +
                "\n" +
                "h1 + p, h2 + p, h3 + p {\n" +
                "\tmargin: 5px 0 10px 0;\n" +
                "}\n" +
                "\n" +
                "hr {\n" +
                "  margin: 5px auto;\n" +
                "  border: 0;\n" +
                "  border-top: 1px solid #bbb;\n" +
                "}\n" +
                "\n" +
                "sup {\n" +
                "\tline-height: 0;\n" +
                "}\n" +
                "\n" +
                ".nowrap {\n" +
                "\twhite-space: nowrap;\n" +
                "} \n" +
                "\n" +
                "/* Structure */\n" +
                "\n" +
                ".header {\n" +
                "\twidth: 250px;\n" +
                "}\n" +
                "\n" +
                ".header-content {\n" +
                "\tfloat: left;\n" +
                "\twidth: 250px;\n" +
                "\ttext-align: center;\n" +
                "}\n" +
                "\n" +
                "#header-mobile {\n" +
                "\tdisplay: none;\n" +
                "}\n" +
                "\n" +
                ".body-content {\n" +
                "\tmargin-left: 250px;\n" +
                "}\n" +
                "\n" +
                ".body {\n" +
                "\tmargin: -15px 10px 10px 10px;\n" +
                "}\n" +
                "\n" +
                ".footer {\n" +
                "\tfont: normal 10px 'Lato', sans-serif;\n" +
                "\ttext-align: center;\n" +
                "\tmargin: 50px 10px 30px 10px;\n" +
                "}\n" +
                "\n" +
                "/* Links/Type */\n" +
                "a {\n" +
                "\tcolor: #33337C;\n" +
                "\ttext-decoration: none;\n" +
                "}\n" +
                "\n" +
                "a:active, a:hover {\n" +
                "\ttext-decoration: underline;\n" +
                "}\n" +
                "\n" +
                "\n" +
                "/* Navigation */\n" +
                "#nav-path {\n" +
                "\tfont: bold 11px 'Lato', sans-serif;\n" +
                "\tcolor: #777;\n" +
                "\ttext-transform: uppercase;\n" +
                "\tpadding: 20px 0 0 10px;\n" +
                "}\n" +
                "\n" +
                "#nav-path a {\n" +
                "\tcolor: #871c1c;\n" +
                "\tpadding: 0 3px;\n" +
                "}\n" +
                "\n" +
                ".nav-menu {\t\n" +
                "\tposition: relative;\n" +
                "\ttop: -30px;\n" +
                "}\n" +
                "\n" +
                ".menu-link {\n" +
                "\tfloat: right;\n" +
                "\tdisplay: block;\n" +
                "\tcolor: #fafafa !important;\n" +
                "\tfont: normal 14px 'Lato', sans-serif;\n" +
                "\tpadding: 0 10px 0 0;\n" +
                "}\n" +
                "\n" +
                ".menu {\n" +
                "\tposition: relative;\n" +
                "\ttop: 12px;\n" +
                "\tfont: normal 14px 'Lato', sans-serif;\n" +
                "}\n" +
                "\n" +
                ".menu, .menu > ul ul {\n" +
                "\tclear: both;\n" +
                "}\n" +
                "\n" +
                ".js .menu, .js .menu > ul ul {\n" +
                "\toverflow: hidden;\n" +
                "\tmax-height: 0;\n" +
                "}\n" +
                "\n" +
                ".menu.active, .js .menu > ul ul.active {\n" +
                "\tmax-height: 12000px;\n" +
                "\tdisplay: block;\n" +
                "}\n" +
                "\n" +
                ".menu a {\n" +
                "\tcolor: #fcfcfc;\n" +
                "}\n" +
                "\n" +
                ".menu ul {\n" +
                "\tmargin: 0;\n" +
                "\tpadding: 0;\n" +
                "\tlist-style: none;\n" +
                "}\n" +
                "\n" +
                ".menu ul li ul li {\n" +
                "\tpadding-left: 10px;\n" +
                "}\n" +
                "\n" +
                ".menu li a {\n" +
                "\tdisplay: block;\n" +
                "\tposition: relative;\n" +
                "\tpadding: 8px;\n" +
                "}\n" +
                "\n" +
                ".menu li {\n" +
                "\tbackground: #70060b;\n" +
                "\tborder-bottom: 1px solid #4d0207;\n" +
                "}\n" +
                "\n" +
                ".level-2 li {\n" +
                "\tbackground: #80171c;\n" +
                "\tborder: 0;\n" +
                "}\n" +
                "\n" +
                ".level-2 .has-subnav {\n" +
                "\tpadding-left: 0;\n" +
                "}\n" +
                "\n" +
                ".level-3 li {\n" +
                "\tbackground: #93242a;\n" +
                "\tpadding-left: 20px !important;\n" +
                "}\n" +
                "\n" +
                ".active-level {\n" +
                "\tfont-style: italic;\n" +
                "\tfont-weight: bold;\n" +
                "}\n" +
                "\n" +
                ".menu li.has-subnav > a:after {\n" +
                "\tcontent: '+';\n" +
                "\tposition: absolute;\n" +
                "\ttop: 0;\n" +
                "\tright: 0;\n" +
                "\tdisplay: block;\n" +
                "\tpadding: 10px;\n" +
                "}\n" +
                "\n" +
                ".menu li.has-subnav > a.active:after {\n" +
                "\tcontent: '-';\n" +
                "}\n" +
                "\n" +
                "@media only screen and (min-width: 800px) {\n" +
                "\t.nav-menu {\n" +
                "\t\tmax-width: 250px;\n" +
                "\t\tmargin: 0;\n" +
                "\t\tfloat: left;\n" +
                "\t\tposition: relative;\n" +
                "\t\ttop: 0;\n" +
                "\t}\n" +
                "\n" +
                "\t.menu {\n" +
                "\t\tposition: relative;\n" +
                "\t\ttop: 0;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.menu-link {\n" +
                "\t\tdisplay: none;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.js .menu, .js .menu > ul ul {\n" +
                "\t\tmax-height: none;\n" +
                "\t\toverflow: visible;\n" +
                "\t\tbackground: none;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.js .menu > ul ul {\n" +
                "\t\tdisplay: none;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.menu > ul {\n" +
                "\t\twidth: 236px;\n" +
                "\t\tmargin: 10px 8px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.menu li.has-subnav > a {\n" +
                "\t\tpadding-right: 2em;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.menu > ul > li, .menu > ul ul li {\n" +
                "\t\tposition: relative;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.menu > ul ul.level-2 {\n" +
                "\t\ttop: 3em;\n" +
                "\t\tleft: 0;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.nav-text {\n" +
                "\t\tfont-size: 12px;\n" +
                "\t\ttext-align: center;\n" +
                "\t\tbackground: none !important;\n" +
                "\t\tborder: 0 !important;\n" +
                "\t\tpadding: 5px 0;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.nav-text a {\n" +
                "\t\tpadding: 0 !important;\n" +
                "\t\tdisplay: inline !important;\n" +
                "\t\tcolor: #33337C;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.search {\n" +
                "\t\tbackground: none !important;\n" +
                "\t\tborder: 0 !important;\n" +
                "\t\tpadding: 0 0 5px 0 !important;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.search input {\n" +
                "\t\twidth: 230px;\n" +
                "\t\tline-height: 18px;\n" +
                "\t\tpadding: 0 0 0 2px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.nav-mobile {\n" +
                "\t\tdisplay: none;\n" +
                "\t}\n" +
                "}\n" +
                "\n" +
                "\n" +
                "/* Tables */\n" +
                "\n" +
                "table {\n" +
                "\tfont-family: 'Lato', sans;\n" +
                "\tborder-collapse: collapse;\n" +
                "\tmargin: 10px 0 20px 0;\n" +
                "\ttext-align: left;\n" +
                "}\n" +
                "\n" +
                "caption {\n" +
                "\tfont: bold 18px 'PT Serif', serif;\n" +
                "}\n" +
                "\n" +
                "th {\n" +
                "\tborder-bottom: 2px solid #585858;\n" +
                "\twhite-space: nowrap;\n" +
                "\tvertical-align: bottom;\n" +
                "\tfont-size: 15px;\n" +
                "\tpadding: 5px 10px 5px 10px;\n" +
                "}\n" +
                "\n" +
                "th:first-child {\n" +
                "\ttext-align: left;\n" +
                "}\n" +
                "\n" +
                "td {\n" +
                "\tvertical-align: top;\n" +
                "\twhite-space: nowrap;\n" +
                "}\n" +
                "\n" +
                "tbody tr:nth-child(odd) td {\n" +
                "\tbackground: rgba(0, 0, 0, 0.05);\n" +
                "}\n" +
                "\n" +
                "tbody tr {\n" +
                "\tborder-bottom: 1px solid rgba(0, 0, 0, 0.08);\n" +
                "}\n" +
                "\n" +
                "tbody th {\n" +
                "\tpadding: 15px 10px 5px 10px;\n" +
                "}\n" +
                "\n" +
                "tbody td {\n" +
                "\tpadding: 5px 10px 5px 10px;\n" +
                "}\n" +
                "\n" +
                "tbody td:first-child {\n" +
                "\twhite-space: nowrap !important;\n" +
                "}\n" +
                "\n" +
                "tfoot {\n" +
                "\tborder-top: 2px solid #ccc;\n" +
                "}\n" +
                "\t\n" +
                "tfoot td {\n" +
                "\tfont-size: 12px;\n" +
                "\tpadding: 5px 0 0 0;\n" +
                "\twhite-space: normal;\n" +
                "}\n" +
                "\n" +
                ".indent-1 {\n" +
                "\ttext-indent: 10px;\n" +
                "}\n" +
                "\n" +
                ".indent-2 {\n" +
                "\ttext-indent: 20px;\n" +
                "}\n" +
                "\n" +
                "/* Stat Blocks */\n" +
                ".monster-header {\n" +
                "\tfont: normal 36px 'PT Serif', serif;\n" +
                "\tfont-variant: small-caps;\n" +
                "\tfont-weight: 600;\n" +
                "\tmargin: 20px 0 10px 0;\n" +
                "}\n" +
                "\n" +
                ".stat-block-title, .stat-block-breaker {\n" +
                "\tfont: normal 16px 'Lato', sans-serif;\n" +
                "\ttext-transform: uppercase;\n" +
                "}\n" +
                "\n" +
                ".stat-block-title {\n" +
                "\tfont-weight: bold;\n" +
                "\tletter-spacing: 1px;\n" +
                "\tbackground-color: black;\n" +
                "\tcolor: white;\n" +
                "\tmargin: 25px 0 5px 0;\n" +
                "\tpadding: 2px 0 2px 5px;\n" +
                "}\n" +
                "\n" +
                ".stat-block-breaker {\n" +
                "\tborder-top: 1px solid black;\n" +
                "\tborder-bottom: 1px solid black;\n" +
                "\tmargin: 8px 0;\n" +
                "}\n" +
                "\n" +
                ".flavor-text {\n" +
                "\tfont-style: italic;\n" +
                "}\n" +
                "\n" +
                ".stat-block, .stat-block-1, .stat-block-2 {\n" +
                "\tfont: normal 13px 'Lato', sans-serif;\n" +
                "\tline-height: 1.5em;\n" +
                "\tmargin: 5px 0;\n" +
                "}\n" +
                "   \t\n" +
                ".stat-block-2 {\n" +
                "\ttext-indent: 25px;\n" +
                "}\n" +
                "\n" +
                ".stat-block-xp {\n" +
                "\tfont: normal 14px 'Lato', sans-serif;\n" +
                "\tfont-weight: bold;\n" +
                "\tmargin: 0;\n" +
                "}\n" +
                "\t\n" +
                ".stat-block-cr {\n" +
                "\tfloat: right;\n" +
                "\tpadding: 0 5px 0 0;\n" +
                "}\n" +
                "\n" +
                ".stat-block-rp {\n" +
                "\tpadding: 0 0 0 10px;\n" +
                "\tfont-weight: bold;\n" +
                "\tfont-style: italic;\n" +
                "}\n" +
                "\n" +
                "/* Indexes */\n" +
                ".shortcut-bar {\n" +
                "\tfont: normal 14px 'Lato', sans-serif;\n" +
                "\ttext-align: center;\n" +
                "\tline-height: 1.75em;\n" +
                "\tborder-bottom: 1px solid #ccc;\n" +
                "\tpadding: 0 0 10px 0;\n" +
                "}\n" +
                "\n" +
                ".shortcut-bar-full {\n" +
                "\tborder-top: 1px solid #ccc;\n" +
                "}\n" +
                "\n" +
                ".shortcut-bar a {\n" +
                "\tpadding: 0 5px;\n" +
                "\tfont-weight: bold;\n" +
                "}\n" +
                "\n" +
                ".shortcut-bar span {\n" +
                "\tpadding: 0 5px;\n" +
                "}\n" +
                "\n" +
                ".index, .spell-list-index {\n" +
                "\tdisplay: block; \n" +
                "\twidth: 100%;\n" +
                "}\n" +
                "\n" +
                ".index ul, .spell-list-index ul {\n" +
                "\tdisplay: block;\n" +
                "\tpadding: 0;\n" +
                "\tlist-style: none;\n" +
                "}\n" +
                "\n" +
                ".index ul:before, .spell-list-index ul:before {\n" +
                "\tdisplay: block;\n" +
                "\twidth: 100%;\n" +
                "\tcontent: attr(title);\n" +
                "}\n" +
                "\n" +
                ".index ul {\n" +
                "\t-moz-column-width: 200px;\n" +
                "\t-moz-column-gap: 5px;\n" +
                "\t-webkit-column-width: 200px;\n" +
                "\t-webkit-column-gap: 5px;\n" +
                "\t-o-column-width: 200px;\n" +
                "\t-o-column-gap: 5px;\n" +
                "\tcolumn-gap: 10px;\n" +
                "\tcolumn-width: 200px;\n" +
                "}\n" +
                "\n" +
                ".index ul:before {\n" +
                "\t-moz-column-span: all;\n" +
                "\t-webkit-column-span: all;\n" +
                "\t-o-column-span: all;\n" +
                "\tcolumn-span: all;\n" +
                "}\n" +
                "\n" +
                ".index li, .spell-list-index li {\n" +
                "\tmargin-left: 2em;\n" +
                "\ttext-indent: -1em;\n" +
                "}\n" +
                "\n" +
                ".index a {\n" +
                "\tcolumn-break-inside: avoid;\n" +
                "\tbreak-inside: avoid;\n" +
                "\t-moz-column-break-inside: avoid;\n" +
                "\t-moz-break-inside: avoid;\n" +
                "\t-webkit-column-break-inside: avoid;\n" +
                "\t-webkit-break-inside: avoid;\n" +
                "\t-o-column-break-inside: avoid;\n" +
                "\t-o-break-inside: avoid;\n" +
                "}\n" +
                "\n" +
                ".index > ul, .spell-list-index > ul {\n" +
                "\tfont-size: 18px;\n" +
                "}\n" +
                "\n" +
                ".index > ul li, .spell-list-index > ul li {\n" +
                "\tfont-size: 14px;\n" +
                "}\n" +
                "\n" +
                "/* Mobile & Small Screens */\n" +
                "@media only screen and (max-width: 800px) {\n" +
                "\tbody {\n" +
                "\t\tbackground: #fffefc url('/pathfinderRPG/prd/include/bg-mobile-new.png') top right no-repeat;\n" +
                "\t}\n" +
                "\t\t\n" +
                "\t.header {\n" +
                "\t\twidth: 100%;\n" +
                "\t\tclear: both; }\n" +
                "\t\t\n" +
                "\t.header-content {\n" +
                "\t\ttext-align: center;\n" +
                "\t\tbackground: #660308;\n" +
                "\t\tbackground-image: -ms-linear-gradient(top, #660308 0%, #4d0105 100%);\n" +
                "\t\tbackground-image: -moz-linear-gradient(top, #660308 0%, #4d0105 100%);\n" +
                "\t\tbackground-image: -o-linear-gradient(top, #660308 0%, #4d0105 100%);\n" +
                "\t\tbackground-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #660308), color-stop(1, #4d0105));\n" +
                "\t\tbackground-image: -webkit-linear-gradient(top, #660308 0%, #4d0105 100%);\n" +
                "\t\tbackground-image: linear-gradient(to bottom, #660308 0%, #4d0105 100%);\n" +
                "\t\tbackground-repeat: repeat-x;\n" +
                "\t\tborder-bottom: 1px solid #330003;\n" +
                "\t\t-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15);\n" +
                "\t\t-moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15);\n" +
                "\t\tbox-shadow: 0 0 2px #ccc;\n" +
                "\t\theight: 44px;\n" +
                "\t\twidth: 100%;\n" +
                "\t}\n" +
                "\t\n" +
                "\t\tdisplay: none;\n" +
                "\t#header-full {\n" +
                "\t}\n" +
                "\t\n" +
                "\t#header-mobile {\n" +
                "\t\tdisplay: block;\n" +
                "\t\ttext-align: left;\n" +
                "\t\tcolor: #f8f6e7;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.body-content {\n" +
                "\t\tmargin-left: 0 !important;\n" +
                "\t\tmargin-top: -5px !important;\n" +
                "\t}\n" +
                "\t\n" +
                "\ttd, th, tbody td:first-child {\n" +
                "\t\twhite-space: normal !important;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.search input {\n" +
                "\t\twidth: 95%;\n" +
                "\t\tline-height: 25px;\n" +
                "\t\tpadding: 0 0 0 5px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.search {\n" +
                "\t\tpadding: 10px 0 !important;\n" +
                "\t\ttext-align: center;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.nav-text {\n" +
                "\t\tdisplay: none;\n" +
                "\t}\n" +
                "\t\n" +
                "\t#nav-path {\n" +
                "\t\tpadding: 5px 0 0 10px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.nav-mobile {\n" +
                "\t\tdisplay: block;\n" +
                "\t}\n" +
                "}\n" +
                "\n" +
                "@media only screen and (max-width: 600px) {\n" +
                "\t.wrapper {\n" +
                "\t\tmax-height: 300px;\n" +
                "\t\tmargin: 15px 5px;\n" +
                "\t}\n" +
                "\t\n" +
                "\tcaption {\n" +
                "\t\ttext-align: left;\n" +
                "\t}\n" +
                "}\n" +
                "</style>\n" +
                "<h1 id=\"monster\">"+ NPC.getName()+"</h1>\n" +
                "\t\t\t\n" +
                "<p class=\"flavor-text\"><i>"+ NPC.getDescription_Visual()+"</i></p>\n" +
                "\n" +
                "<p class=\"stat-block-title\"><b>" + NPC.getName() + " <span class=stat-block-cr>CR "+ NPC.getCR()+"</span></b></p>\n" +
                "<p class=\"stat-block-1\"><b>XP "+ NPC.getXP()+"</b></p>\n" +
                "<p class=\"stat-block-1\">"+ NPC.getAlignment()+" " + NPC.getSize() + "  <a href=\"creatureTypes.html\" >"+ NPC.getType()+"</a> (<a href=\"creatureTypes.html#evil-subtype\" >"+ NPC.getSubType()+"</a>)</p>\n" +
                "<p class=\"stat-block-1\"><b>Init</b> "+ NPC.getInit()+"; <b>Senses</b> "+ NPC.getSenses()+"</p>\n" +
                "<p class = \"stat-block-breaker\">Defense</p>\n" +
                "<p class=\"stat-block-1\"><b>AC</b> "+ NPC.getAC()+"</p>\n" +
                "<p class=\"stat-block-1\"><b>hp</b> "+ NPC.getHP()+" "+ NPC.getHD()+" "+ (NPC.getHP_Mods())+"</p>\n" +
                "<p class=\"stat-block-1\">"+ NPC.getSaves()+"</p>\n" +
                (NPC.getDR() == null || NPC.getDR().isEmpty() ? "" : "<p class=\"stat-block-1\"><b>DR</b> "+ NPC.getDR()+"</p>\n") +
                "<p class = \"stat-block-breaker\">Offense</p>\n" +
                "<p class=\"stat-block-1\"><b>Speed</b> "+ NPC.getSpeed()+"</p>\n" +
                "<p class=\"stat-block-1\"><b>Melee</b> "+ NPC.getMelee() + "</p>\n" +
                "<p class=\"stat-block-1\"><b>Ranged</b> "+ NPC.getRanged() + "</p>\n" +
                (NPC.getSpecialAttacks() == null || NPC.getSpecialAttacks().isEmpty() ? "" : "<p class=\"stat-block-1\"><b>Special Attacks</b> "+ NPC.getSpecialAttacks()+"</p>\n") +
                "<p class=\"stat-block-1\"><b>Spell-Like Abilities</b> </p>\n" +
                "<p class=\"stat-block-2\">"+ NPC.getSpellLikeAbilities()+"</p>\n" +
                (NPC.getSpellsKnown() == null || NPC.getSpellsKnown().isEmpty() ? "" : "<p class=\"stat-block-1\"><b>Spells known</b> "+ NPC.getSpellsKnown()+"</p>\n") +
                (NPC.getSpellsPrepared() == null || NPC.getSpellsPrepared().isEmpty() ? "" : "<p class=\"stat-block-1\"><b>Spells prepared</b> "+ NPC.getSpellsPrepared()+"</p>\n") +
                "<p class = \"stat-block-breaker\">Statistics</p>\n" +
                "<p class=\"stat-block-1\">"+ NPC.getBaseStatistics()+"</p>\n" +
                "<p class=\"stat-block-1\"><b>Base</b> <b>Atk</b> "+ NPC.getBaseAtk()+"; <b>CMB</b> "+ NPC.getCMB()+"; <b>CMD</b> "+ NPC.getCMD()+"</p>\n" +
                "<p class=\"stat-block-1\"><b>Feats</b> "+ NPC.getFeats()+"</p>\n" +
                "<p class=\"stat-block-1\"><b>Skills</b> "+ NPC.getSkills()+"</p>\n" +
                "<p class=\"stat-block-1\"><b>Languages</b> "+ NPC.getLanguages()+"</p>\n" +
                "<p class=\"stat-block-1\"><b>SQ</b> "+ NPC.getSQ()+"</p>\n" +
                "<p class = \"stat-block-breaker\">Tactics</p>\n" +
                "<p class=\"stat-block-1\"><b>Before combat</b> "+ NPC.getBeforeCombat()+"</p>\n" +
                "<p class=\"stat-block-1\"><b>During combat</b> "+ NPC.getDuringCombat()+"</p>\n" +
                "<p class=\"stat-block-1\"><b>During combat</b> "+ NPC.getMorale()+"</p>\n" +
                "<p class = \"stat-block-breaker\">Ecology</p>\n" +
                "<p class=\"stat-block-1\"><b>Environment </b>"+ NPC.getEnvironment()+"</p>\n" +
                "<p class=\"stat-block-1\"><b>Organization</b> "+ NPC.getOrganization()+"</p>\n" +
                "<p class=\"stat-block-1\"><b>Treasure</b> "+ NPC.getTreasure()+"</p>\n" +
                "<p class = \"stat-block-breaker\">Special Abilities</p>\n" +
                "<p class=\"stat-block-1\">"+ NPC.getSpecialAbilities()+"</p>\n" +
                "<p>"+ NPC.getDescription()+"</p>";
    }

    public static String getFeatHtml(Feat newValue) {



        return "<style>\n" +
                "@import url(http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|PT+Serif:400,700,400italic,700italic);\n" +
                "\n" +
                "\t.wrapper {\n" +
                "\t\tposition: relative;\n" +
                "\t\toverflow: auto;\n" +
                "\t\tmargin: 5px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.scroller {\n" +
                "\t\tpadding: 10px;\n" +
                "\t\tmax-width: 1000px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.scroller table {\n" +
                "\t\twidth: 100%;\n" +
                "\t}\n" +
                "\t\n" +
                "/* Global */\n" +
                "body, img {\n" +
                "\tmargin: 0;\n" +
                "\tpadding: 0;\n" +
                "\tborder: 0;\n" +
                "}\n" +
                "\n" +
                "body {\n" +
                "\tfont-size: 14px;\n" +
                "\tfont-family: 'PT Serif', serif;\n" +
                "\tbackground: #fffdf8 url('/pathfinderRPG/prd/include/bg-new.png') top left no-repeat;\n" +
                "}\n" +
                "\n" +
                "p {\n" +
                "\tline-height: 1.4em;\n" +
                "\tmargin: 15px 0;\n" +
                "}\n" +
                "\n" +
                "h1 {\n" +
                "\tfont: normal 32px 'PT Serif', serif;\n" +
                "\tmargin: 30px 0 0 0;\n" +
                "}\n" +
                "\t\n" +
                "h2 {\n" +
                "\tfont: normal 26px 'PT Serif', serif;\n" +
                "\tmargin: 30px 0 0 0;\n" +
                "}\n" +
                "\n" +
                "h3 {\n" +
                "\tfont: normal 20px 'PT Serif', serif;\n" +
                "\tmargin: 30px 0 0 0;\n" +
                "}\n" +
                "\n" +
                "h1 + p, h2 + p, h3 + p {\n" +
                "\tmargin: 5px 0 10px 0;\n" +
                "}\n" +
                "\n" +
                "hr {\n" +
                "  margin: 5px auto;\n" +
                "  border: 0;\n" +
                "  border-top: 1px solid #bbb;\n" +
                "}\n" +
                "\n" +
                "sup {\n" +
                "\tline-height: 0;\n" +
                "}\n" +
                "\n" +
                ".nowrap {\n" +
                "\twhite-space: nowrap;\n" +
                "} \n" +
                "\n" +
                "/* Structure */\n" +
                "\n" +
                ".header {\n" +
                "\twidth: 250px;\n" +
                "}\n" +
                "\n" +
                ".header-content {\n" +
                "\tfloat: left;\n" +
                "\twidth: 250px;\n" +
                "\ttext-align: center;\n" +
                "}\n" +
                "\n" +
                "#header-mobile {\n" +
                "\tdisplay: none;\n" +
                "}\n" +
                "\n" +
                ".body-content {\n" +
                "\tmargin-left: 250px;\n" +
                "}\n" +
                "\n" +
                ".body {\n" +
                "\tmargin: -15px 10px 10px 10px;\n" +
                "}\n" +
                "\n" +
                ".footer {\n" +
                "\tfont: normal 10px 'Lato', sans-serif;\n" +
                "\ttext-align: center;\n" +
                "\tmargin: 50px 10px 30px 10px;\n" +
                "}\n" +
                "\n" +
                "/* Links/Type */\n" +
                "a {\n" +
                "\tcolor: #33337C;\n" +
                "\ttext-decoration: none;\n" +
                "}\n" +
                "\n" +
                "a:active, a:hover {\n" +
                "\ttext-decoration: underline;\n" +
                "}\n" +
                "\n" +
                "\n" +
                "/* Navigation */\n" +
                "#nav-path {\n" +
                "\tfont: bold 11px 'Lato', sans-serif;\n" +
                "\tcolor: #777;\n" +
                "\ttext-transform: uppercase;\n" +
                "\tpadding: 20px 0 0 10px;\n" +
                "}\n" +
                "\n" +
                "#nav-path a {\n" +
                "\tcolor: #871c1c;\n" +
                "\tpadding: 0 3px;\n" +
                "}\n" +
                "\n" +
                ".nav-menu {\t\n" +
                "\tposition: relative;\n" +
                "\ttop: -30px;\n" +
                "}\n" +
                "\n" +
                ".menu-link {\n" +
                "\tfloat: right;\n" +
                "\tdisplay: block;\n" +
                "\tcolor: #fafafa !important;\n" +
                "\tfont: normal 14px 'Lato', sans-serif;\n" +
                "\tpadding: 0 10px 0 0;\n" +
                "}\n" +
                "\n" +
                ".menu {\n" +
                "\tposition: relative;\n" +
                "\ttop: 12px;\n" +
                "\tfont: normal 14px 'Lato', sans-serif;\n" +
                "}\n" +
                "\n" +
                ".menu, .menu > ul ul {\n" +
                "\tclear: both;\n" +
                "}\n" +
                "\n" +
                ".js .menu, .js .menu > ul ul {\n" +
                "\toverflow: hidden;\n" +
                "\tmax-height: 0;\n" +
                "}\n" +
                "\n" +
                ".menu.active, .js .menu > ul ul.active {\n" +
                "\tmax-height: 12000px;\n" +
                "\tdisplay: block;\n" +
                "}\n" +
                "\n" +
                ".menu a {\n" +
                "\tcolor: #fcfcfc;\n" +
                "}\n" +
                "\n" +
                ".menu ul {\n" +
                "\tmargin: 0;\n" +
                "\tpadding: 0;\n" +
                "\tlist-style: none;\n" +
                "}\n" +
                "\n" +
                ".menu ul li ul li {\n" +
                "\tpadding-left: 10px;\n" +
                "}\n" +
                "\n" +
                ".menu li a {\n" +
                "\tdisplay: block;\n" +
                "\tposition: relative;\n" +
                "\tpadding: 8px;\n" +
                "}\n" +
                "\n" +
                ".menu li {\n" +
                "\tbackground: #70060b;\n" +
                "\tborder-bottom: 1px solid #4d0207;\n" +
                "}\n" +
                "\n" +
                ".level-2 li {\n" +
                "\tbackground: #80171c;\n" +
                "\tborder: 0;\n" +
                "}\n" +
                "\n" +
                ".level-2 .has-subnav {\n" +
                "\tpadding-left: 0;\n" +
                "}\n" +
                "\n" +
                ".level-3 li {\n" +
                "\tbackground: #93242a;\n" +
                "\tpadding-left: 20px !important;\n" +
                "}\n" +
                "\n" +
                ".active-level {\n" +
                "\tfont-style: italic;\n" +
                "\tfont-weight: bold;\n" +
                "}\n" +
                "\n" +
                ".menu li.has-subnav > a:after {\n" +
                "\tcontent: '+';\n" +
                "\tposition: absolute;\n" +
                "\ttop: 0;\n" +
                "\tright: 0;\n" +
                "\tdisplay: block;\n" +
                "\tpadding: 10px;\n" +
                "}\n" +
                "\n" +
                ".menu li.has-subnav > a.active:after {\n" +
                "\tcontent: '-';\n" +
                "}\n" +
                "\n" +
                "@media only screen and (min-width: 800px) {\n" +
                "\t.nav-menu {\n" +
                "\t\tmax-width: 250px;\n" +
                "\t\tmargin: 0;\n" +
                "\t\tfloat: left;\n" +
                "\t\tposition: relative;\n" +
                "\t\ttop: 0;\n" +
                "\t}\n" +
                "\n" +
                "\t.menu {\n" +
                "\t\tposition: relative;\n" +
                "\t\ttop: 0;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.menu-link {\n" +
                "\t\tdisplay: none;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.js .menu, .js .menu > ul ul {\n" +
                "\t\tmax-height: none;\n" +
                "\t\toverflow: visible;\n" +
                "\t\tbackground: none;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.js .menu > ul ul {\n" +
                "\t\tdisplay: none;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.menu > ul {\n" +
                "\t\twidth: 236px;\n" +
                "\t\tmargin: 10px 8px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.menu li.has-subnav > a {\n" +
                "\t\tpadding-right: 2em;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.menu > ul > li, .menu > ul ul li {\n" +
                "\t\tposition: relative;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.menu > ul ul.level-2 {\n" +
                "\t\ttop: 3em;\n" +
                "\t\tleft: 0;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.nav-text {\n" +
                "\t\tfont-size: 12px;\n" +
                "\t\ttext-align: center;\n" +
                "\t\tbackground: none !important;\n" +
                "\t\tborder: 0 !important;\n" +
                "\t\tpadding: 5px 0;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.nav-text a {\n" +
                "\t\tpadding: 0 !important;\n" +
                "\t\tdisplay: inline !important;\n" +
                "\t\tcolor: #33337C;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.search {\n" +
                "\t\tbackground: none !important;\n" +
                "\t\tborder: 0 !important;\n" +
                "\t\tpadding: 0 0 5px 0 !important;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.search input {\n" +
                "\t\twidth: 230px;\n" +
                "\t\tline-height: 18px;\n" +
                "\t\tpadding: 0 0 0 2px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.nav-mobile {\n" +
                "\t\tdisplay: none;\n" +
                "\t}\n" +
                "}\n" +
                "\n" +
                "\n" +
                "/* Tables */\n" +
                "\n" +
                "table {\n" +
                "\tfont-family: 'Lato', sans;\n" +
                "\tborder-collapse: collapse;\n" +
                "\tmargin: 10px 0 20px 0;\n" +
                "\ttext-align: left;\n" +
                "}\n" +
                "\n" +
                "caption {\n" +
                "\tfont: bold 18px 'PT Serif', serif;\n" +
                "}\n" +
                "\n" +
                "th {\n" +
                "\tborder-bottom: 2px solid #585858;\n" +
                "\twhite-space: nowrap;\n" +
                "\tvertical-align: bottom;\n" +
                "\tfont-size: 15px;\n" +
                "\tpadding: 5px 10px 5px 10px;\n" +
                "}\n" +
                "\n" +
                "th:first-child {\n" +
                "\ttext-align: left;\n" +
                "}\n" +
                "\n" +
                "td {\n" +
                "\tvertical-align: top;\n" +
                "\twhite-space: nowrap;\n" +
                "}\n" +
                "\n" +
                "tbody tr:nth-child(odd) td {\n" +
                "\tbackground: rgba(0, 0, 0, 0.05);\n" +
                "}\n" +
                "\n" +
                "tbody tr {\n" +
                "\tborder-bottom: 1px solid rgba(0, 0, 0, 0.08);\n" +
                "}\n" +
                "\n" +
                "tbody th {\n" +
                "\tpadding: 15px 10px 5px 10px;\n" +
                "}\n" +
                "\n" +
                "tbody td {\n" +
                "\tpadding: 5px 10px 5px 10px;\n" +
                "}\n" +
                "\n" +
                "tbody td:first-child {\n" +
                "\twhite-space: nowrap !important;\n" +
                "}\n" +
                "\n" +
                "tfoot {\n" +
                "\tborder-top: 2px solid #ccc;\n" +
                "}\n" +
                "\t\n" +
                "tfoot td {\n" +
                "\tfont-size: 12px;\n" +
                "\tpadding: 5px 0 0 0;\n" +
                "\twhite-space: normal;\n" +
                "}\n" +
                "\n" +
                ".indent-1 {\n" +
                "\ttext-indent: 10px;\n" +
                "}\n" +
                "\n" +
                ".indent-2 {\n" +
                "\ttext-indent: 20px;\n" +
                "}\n" +
                "\n" +
                "/* Stat Blocks */\n" +
                ".monster-header {\n" +
                "\tfont: normal 36px 'PT Serif', serif;\n" +
                "\tfont-variant: small-caps;\n" +
                "\tfont-weight: 600;\n" +
                "\tmargin: 20px 0 10px 0;\n" +
                "}\n" +
                "\n" +
                ".stat-block-title, .stat-block-breaker {\n" +
                "\tfont: normal 16px 'Lato', sans-serif;\n" +
                "\ttext-transform: uppercase;\n" +
                "}\n" +
                "\n" +
                ".stat-block-title {\n" +
                "\tfont-weight: bold;\n" +
                "\tletter-spacing: 1px;\n" +
                "\tbackground-color: black;\n" +
                "\tcolor: white;\n" +
                "\tmargin: 25px 0 5px 0;\n" +
                "\tpadding: 2px 0 2px 5px;\n" +
                "}\n" +
                "\n" +
                ".stat-block-breaker {\n" +
                "\tborder-top: 1px solid black;\n" +
                "\tborder-bottom: 1px solid black;\n" +
                "\tmargin: 8px 0;\n" +
                "}\n" +
                "\n" +
                ".flavor-text {\n" +
                "\tfont-style: italic;\n" +
                "}\n" +
                "\n" +
                ".stat-block, .stat-block-1, .stat-block-2 {\n" +
                "\tfont: normal 13px 'Lato', sans-serif;\n" +
                "\tline-height: 1.5em;\n" +
                "\tmargin: 5px 0;\n" +
                "}\n" +
                "   \t\n" +
                ".stat-block-2 {\n" +
                "\ttext-indent: 25px;\n" +
                "}\n" +
                "\n" +
                ".stat-block-xp {\n" +
                "\tfont: normal 14px 'Lato', sans-serif;\n" +
                "\tfont-weight: bold;\n" +
                "\tmargin: 0;\n" +
                "}\n" +
                "\t\n" +
                ".stat-block-cr {\n" +
                "\tfloat: right;\n" +
                "\tpadding: 0 5px 0 0;\n" +
                "}\n" +
                "\n" +
                ".stat-block-rp {\n" +
                "\tpadding: 0 0 0 10px;\n" +
                "\tfont-weight: bold;\n" +
                "\tfont-style: italic;\n" +
                "}\n" +
                "\n" +
                "/* Indexes */\n" +
                ".shortcut-bar {\n" +
                "\tfont: normal 14px 'Lato', sans-serif;\n" +
                "\ttext-align: center;\n" +
                "\tline-height: 1.75em;\n" +
                "\tborder-bottom: 1px solid #ccc;\n" +
                "\tpadding: 0 0 10px 0;\n" +
                "}\n" +
                "\n" +
                ".shortcut-bar-full {\n" +
                "\tborder-top: 1px solid #ccc;\n" +
                "}\n" +
                "\n" +
                ".shortcut-bar a {\n" +
                "\tpadding: 0 5px;\n" +
                "\tfont-weight: bold;\n" +
                "}\n" +
                "\n" +
                ".shortcut-bar span {\n" +
                "\tpadding: 0 5px;\n" +
                "}\n" +
                "\n" +
                ".index, .spell-list-index {\n" +
                "\tdisplay: block; \n" +
                "\twidth: 100%;\n" +
                "}\n" +
                "\n" +
                ".index ul, .spell-list-index ul {\n" +
                "\tdisplay: block;\n" +
                "\tpadding: 0;\n" +
                "\tlist-style: none;\n" +
                "}\n" +
                "\n" +
                ".index ul:before, .spell-list-index ul:before {\n" +
                "\tdisplay: block;\n" +
                "\twidth: 100%;\n" +
                "\tcontent: attr(title);\n" +
                "}\n" +
                "\n" +
                ".index ul {\n" +
                "\t-moz-column-width: 200px;\n" +
                "\t-moz-column-gap: 5px;\n" +
                "\t-webkit-column-width: 200px;\n" +
                "\t-webkit-column-gap: 5px;\n" +
                "\t-o-column-width: 200px;\n" +
                "\t-o-column-gap: 5px;\n" +
                "\tcolumn-gap: 10px;\n" +
                "\tcolumn-width: 200px;\n" +
                "}\n" +
                "\n" +
                ".index ul:before {\n" +
                "\t-moz-column-span: all;\n" +
                "\t-webkit-column-span: all;\n" +
                "\t-o-column-span: all;\n" +
                "\tcolumn-span: all;\n" +
                "}\n" +
                "\n" +
                ".index li, .spell-list-index li {\n" +
                "\tmargin-left: 2em;\n" +
                "\ttext-indent: -1em;\n" +
                "}\n" +
                "\n" +
                ".index a {\n" +
                "\tcolumn-break-inside: avoid;\n" +
                "\tbreak-inside: avoid;\n" +
                "\t-moz-column-break-inside: avoid;\n" +
                "\t-moz-break-inside: avoid;\n" +
                "\t-webkit-column-break-inside: avoid;\n" +
                "\t-webkit-break-inside: avoid;\n" +
                "\t-o-column-break-inside: avoid;\n" +
                "\t-o-break-inside: avoid;\n" +
                "}\n" +
                "\n" +
                ".index > ul, .spell-list-index > ul {\n" +
                "\tfont-size: 18px;\n" +
                "}\n" +
                "\n" +
                ".index > ul li, .spell-list-index > ul li {\n" +
                "\tfont-size: 14px;\n" +
                "}\n" +
                "\n" +
                "/* Mobile & Small Screens */\n" +
                "@media only screen and (max-width: 800px) {\n" +
                "\tbody {\n" +
                "\t\tbackground: #fffefc url('/pathfinderRPG/prd/include/bg-mobile-new.png') top right no-repeat;\n" +
                "\t}\n" +
                "\t\t\n" +
                "\t.header {\n" +
                "\t\twidth: 100%;\n" +
                "\t\tclear: both; }\n" +
                "\t\t\n" +
                "\t.header-content {\n" +
                "\t\ttext-align: center;\n" +
                "\t\tbackground: #660308;\n" +
                "\t\tbackground-image: -ms-linear-gradient(top, #660308 0%, #4d0105 100%);\n" +
                "\t\tbackground-image: -moz-linear-gradient(top, #660308 0%, #4d0105 100%);\n" +
                "\t\tbackground-image: -o-linear-gradient(top, #660308 0%, #4d0105 100%);\n" +
                "\t\tbackground-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #660308), color-stop(1, #4d0105));\n" +
                "\t\tbackground-image: -webkit-linear-gradient(top, #660308 0%, #4d0105 100%);\n" +
                "\t\tbackground-image: linear-gradient(to bottom, #660308 0%, #4d0105 100%);\n" +
                "\t\tbackground-repeat: repeat-x;\n" +
                "\t\tborder-bottom: 1px solid #330003;\n" +
                "\t\t-webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15);\n" +
                "\t\t-moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15);\n" +
                "\t\tbox-shadow: 0 0 2px #ccc;\n" +
                "\t\theight: 44px;\n" +
                "\t\twidth: 100%;\n" +
                "\t}\n" +
                "\t\n" +
                "\t\tdisplay: none;\n" +
                "\t#header-full {\n" +
                "\t}\n" +
                "\t\n" +
                "\t#header-mobile {\n" +
                "\t\tdisplay: block;\n" +
                "\t\ttext-align: left;\n" +
                "\t\tcolor: #f8f6e7;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.body-content {\n" +
                "\t\tmargin-left: 0 !important;\n" +
                "\t\tmargin-top: -5px !important;\n" +
                "\t}\n" +
                "\t\n" +
                "\ttd, th, tbody td:first-child {\n" +
                "\t\twhite-space: normal !important;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.search input {\n" +
                "\t\twidth: 95%;\n" +
                "\t\tline-height: 25px;\n" +
                "\t\tpadding: 0 0 0 5px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.search {\n" +
                "\t\tpadding: 10px 0 !important;\n" +
                "\t\ttext-align: center;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.nav-text {\n" +
                "\t\tdisplay: none;\n" +
                "\t}\n" +
                "\t\n" +
                "\t#nav-path {\n" +
                "\t\tpadding: 5px 0 0 10px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.nav-mobile {\n" +
                "\t\tdisplay: block;\n" +
                "\t}\n" +
                "}\n" +
                "\n" +
                "@media only screen and (max-width: 600px) {\n" +
                "\t.wrapper {\n" +
                "\t\tmax-height: 300px;\n" +
                "\t\tmargin: 15px 5px;\n" +
                "\t}\n" +
                "\t\n" +
                "\tcaption {\n" +
                "\t\ttext-align: left;\n" +
                "\t}\n" +
                "}\n" +
                "</style>\n" +
                newValue.getFulltext();


             /*   "<h2>"+newValue.getName()+ " ("+newValue.getType()+")</h2>\n" +
                "<p>"+newValue.getDescription()+"</p>\n" +
                ( newValue.getPrerequisites() == null ||  newValue.getPrerequisites().isEmpty() ? "" : "<p class=\"stat-block-1\"><b>Prerequisite:</b> "+ newValue.getPrerequisites()+"</p>\n" )+
                "<p class=\"stat-block-1\"><b>Benefit:</b> "+newValue.getBenefit()+"</p>\n" +
                "<p class=\"stat-block-1\"><b>Normal:</b> "+ newValue.getNormal()+"</p>\n" +
                "<p class=\"stat-block-1\"><b>Special:</b> "+newValue.getSpecial()+"</p>\n";*/
    }

    public static String getMagicItemHtml(MagicItem newValue) {
        return newValue.getFullText();
    }

    public static String getSpellHtml(Spell newValue) {
        return newValue.getFull_text();
    }

        public static String getHtml(String encounter) {
                return "<div>" + encounter + "</div>";
        }
}
