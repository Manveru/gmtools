package data.notes;

import java.io.Serializable;

/**
 * Created by Michel on 31-3-2015.
 */
public class Relation implements Serializable {
    private String title;
    private int id;
    private RelationEnum relationType;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public RelationEnum getRelationType() {
        return relationType;
    }

    public void setRelationType(RelationEnum relationType) {
        this.relationType = relationType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
