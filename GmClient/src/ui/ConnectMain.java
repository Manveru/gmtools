package ui;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Created by Michel on 28-3-2015.
 */
public class ConnectMain extends Application {

    private TextField field;

    @Override
    public void start(Stage stage) {


        Scene scene = new Scene(addGridPane());
        stage.setScene(scene);
        //stage.setFullScreen(true);
        stage.setTitle("GM tools client");

        stage.show();

    }

    private void doStuff() {


        MainWindow window = new MainWindow(field.getText());


        Scene secondScene = new Scene(window);

        Stage secondStage = new Stage();
        secondStage.setFullScreen(true);
        secondStage.setTitle("Client preview");
        secondStage.setScene(secondScene);
        secondStage.show();


    }

    public GridPane addGridPane() {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(0, 10, 0, 10));

        Text pcgenFileslabel = new Text("IP address:");
        pcgenFileslabel.setFont(Font.font("Arial", FontWeight.BOLD, 13));
        grid.add(pcgenFileslabel, 0, 0, 2, 1);

        field = new TextField("127.0.0.1");
        field.setFont(Font.font("Arial", FontWeight.BOLD, 13));
        grid.add(field, 0, 1, 4, 1);

        Button start = new Button("Start");
        start.setOnAction(e -> doStuff());
        grid.add(start, 1, 2, 4, 1);
        return grid;
    }

}
