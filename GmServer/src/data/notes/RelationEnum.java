package data.notes;

/**
 * Created by Michel on 31-3-2015.
 */
public enum RelationEnum {
    FEAT,
    MAGICITEM,
    NPC,
    MONSTER,
    SPELL
}
