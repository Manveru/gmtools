package data;

/**
 * Created by Michel on 17-3-2015.
 */
public class PcgFile
{
    String fullText;
    String bio;
    String description;

    public PcgFile(String fullText) {
        this.fullText = fullText;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;


    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public String getBio() {
        return bio.replace("&nl;", "\n").replace("&colon;", ":");
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getDescription() {
        return description.replace("&nl;", "\n").replace("&colon;", ":");
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
