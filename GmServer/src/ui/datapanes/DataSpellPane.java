package ui.datapanes;

import data.Spell;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Callback;
import ui.HtmlGenerator;

import java.util.ArrayList;

/**
 * Created by Michel on 29-3-2015.
 */
public class DataSpellPane extends BorderPane {
    public DataSpellPane(ArrayList<Spell> spells) {
        ListView<Spell> listView = new ListView<>();

        TextField searchMonster = new TextField();
        final WebView browser = new WebView();
        final WebEngine webEngine = browser.getEngine();

        //monsterText.setDisable(true);

        setCenter(browser);
        ObservableList<Spell> myObservableList = FXCollections.observableList(spells);

        FilteredList<Spell> filteredData = new FilteredList<>(myObservableList, p -> true);


        listView.setCellFactory(new Callback<ListView<Spell>, ListCell<Spell>>() {

            @Override
            public ListCell<Spell> call(ListView<Spell> p) {

                ListCell<Spell> cell = new ListCell<Spell>() {

                    @Override
                    protected void updateItem(Spell t, boolean bln) {
                        super.updateItem(t, bln);
                        if (t != null) {
                            setText(t.getName());
                        } else {
                            setText(null);
                        }
                    }

                };

                return cell;
            }
        });

        VBox vbox = new VBox(10);
        VBox.setVgrow(listView, Priority.ALWAYS);
        vbox.getChildren().addAll(searchMonster, listView);
        setLeft(vbox);

        searchMonster.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(spell -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (spell.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                }
                return false; // Does not match.
            });
        });

        listView.setItems(filteredData);

        listView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                webEngine.loadContent(HtmlGenerator.getSpellHtml(newValue));
            }
        });


    }
}
