package controllers;

import base.*;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import ui.ImageSelection;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Controller
{
    private Server kryoServer = null;
    private boolean waitingForAnswer = false;
    private ImageSelection currentImageToSend;
    private int UUID = 0;

    public boolean StartServer()
    {
        try {
            kryoServer = new Server(50000,50000);
            kryoServer.start();
            kryoServer.bind(54555, 54777);

            Kryo kryo = kryoServer.getKryo();
            kryo.register(String.class);
            kryo.register(byte[].class);
            kryo.register(TranslateObject.class);
            kryo.register(HashCode.class);
            kryo.register(ZoomObject.class);
            kryo.register(BytePart.class);
            kryo.register(RectObject.class);
            kryo.register(Double[].class);

            kryoServer.addListener(new Listener() {
                public void received (Connection connection, Object object) {
                    if(object instanceof HashCode)
                    {
                        HashCode hash = (HashCode)object;
                        if(waitingForAnswer && currentImageToSend != null && !hash.clientAlreadyHasImage && hash.hash == currentImageToSend.getHash())
                        {
                            BufferedImage img = getBufferedImage(currentImageToSend.getImage());

                            byte[] array = extractBytes(img);


                            ByteArrayInputStream input = new ByteArrayInputStream(array);
                            // Send data in 512 byte chunks.
                            connection.addListener(new InputStreamImageSender(input, 8192) {
                                protected void start() {
                                    // Normally would send an object so the receiving side knows how to handle the chunks we are about to send.
                                    connection.sendTCP("start");

                                }

                                protected Object next(byte[] bytes) {
                                    if (bytes == null) {
                                        return "stop";
                                    }

                                    BytePart part = new BytePart();
                                    part.part = bytes;
                                    return part; // Normally would wrap the byte[] with an object so the receiving side knows how to handle it.
                                }
                            });

                            waitingForAnswer = false;
                            currentImageToSend = null;
                        }
                        else if(hash.clientAlreadyHasImage)
                        {
                            waitingForAnswer = false;
                            currentImageToSend = null;
                        }
                    }
                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return true;
    }

    public boolean stopServer()
    {
        kryoServer.stop();
        return true;
    }

    public void readCharacters() {

    }

    public BufferedImage getBufferedImage(Image fxImage)
    {
        return SwingFXUtils.fromFXImage(fxImage, null);
    }

    public ImageSelection getImage(String filePath)
    {
        try {
            if (filePath.endsWith(".png") || filePath.endsWith(".jpg")) {
                Image img = new Image("file:"+filePath);
                ImageSelection imageSelection = new ImageSelection(img, filePath);
                imageSelection.setHash(UUID);
                UUID++;
                return imageSelection;
            }
        }
        catch (Exception ex)
        {   ex.printStackTrace();
        }
        return null;
    }
    public byte[] extractBytes (BufferedImage bufferedImage)
    {
        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "png", baos);

            return (baos.toByteArray());
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
    public void sendImage(ImageSelection image)
    {
        if(kryoServer != null)
        {
            currentImageToSend = image;
            HashCode code = new HashCode();
            code.hash = image.getHash();
            kryoServer.sendToAllTCP(code);
            waitingForAnswer = true;


        }
    }


    public List<byte[]> bufferedArray(byte[] array)
    {
        List<byte[]> splitted = new ArrayList<>();
        int lengthToSplit = 15000;

        int arrayLength = array.length;

        for (int i = 0; i < arrayLength; i = i + lengthToSplit)
        {
            byte[] val = new byte[lengthToSplit];

            if (arrayLength < i + lengthToSplit)
            {
                lengthToSplit = arrayLength - i;
            }
            System.arraycopy(array, i, val, 0, lengthToSplit);
            splitted.add(val);
        }
        return splitted;
    }

    public void sendTranslate(double x, double y)
    {
        if(kryoServer != null) {
            TranslateObject translateObject = new TranslateObject();
            translateObject.x = x;
            translateObject.y = y;

            kryoServer.sendToAllUDP(translateObject);
        }
    }

    public void sendZoom(double x, double y)
    {
        if(kryoServer != null) {
            ZoomObject zoomObject = new ZoomObject();
            zoomObject.scaleX = x;
            zoomObject.scaleY = y;

            kryoServer.sendToAllUDP(zoomObject);
        }
    }

    public void sendSubtract(Double[] array) {
        if (kryoServer != null) {
            RectObject obj = new RectObject();
            obj.array = array;
            kryoServer.sendToAllUDP(obj);
        }
    }
}
