package ui.datapanes;

import data.Feat;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Callback;
import ui.HtmlGenerator;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Created by Michel on 29-3-2015.
 */
public class DataFeatPane extends BorderPane
{
    public DataFeatPane(ArrayList<Feat> feats)
    {
        ListView<Feat> listView = new ListView<>();

        TextField searchMonster = new TextField();
        final WebView browser = new WebView();
        final WebEngine webEngine = browser.getEngine();

        //monsterText.setDisable(true);


        setCenter(browser);
        ObservableList<Feat> myObservableList = FXCollections.observableList(feats);
        listView.setItems(myObservableList);

        listView.setCellFactory(new Callback<ListView<Feat>, ListCell<Feat>>(){

            @Override
            public ListCell<Feat> call(ListView<Feat> p) {

                ListCell<Feat> cell = new ListCell<Feat>(){

                    @Override
                    protected void updateItem(Feat t, boolean bln) {
                        super.updateItem(t, bln);
                        if (t != null) {
                            setText(t.getName() + " (" + t.getSource() + ")");
                        } else {
                            setText(null);
                        }
                    }

                };

                return cell;
            }
        });

        VBox vbox = new VBox(10);
        VBox.setVgrow(listView, Priority.ALWAYS);
        vbox.getChildren().addAll(searchMonster, listView);
        setLeft(vbox);

        searchMonster.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(final ObservableValue<? extends String> observable, final String oldValue, final String newValue)
            {
                if(newValue == null || newValue.isEmpty())
                {
                    ObservableList<Feat> myObservableList = FXCollections.observableList(feats);
                    listView.setItems(myObservableList);
                }
                else {
                    ObservableList<Feat> myObservableList = FXCollections.observableList(feats.stream()
                            .filter(p -> p.getName().toLowerCase().contains(newValue.toLowerCase())).collect(Collectors.toList()));
                    listView.setItems(myObservableList);
                }
            }
        });


        listView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null) {
                webEngine.loadContent(HtmlGenerator.getFeatHtml(newValue));
            }
        });




    }
}
