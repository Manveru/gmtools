package controllers;

import base.*;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.shape.Polygon;
import ui.ImageSelection;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Controller {


    List<ImageListener> listeners = new ArrayList<>();
    ArrayList<byte[]> arrayList = new ArrayList<>();
    ArrayList<ImageSelection> imageSelections = new ArrayList<>();
    private ImageSelection currentImage = null;
    private int newUUID = -1;
    private Client client;

    public Controller(String ipadress)
    {
        try {
            client = new Client(50000, 50000);
            client.start();
            client.connect(5000, ipadress, 54555, 54777);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean StartClient() {
        try {
            Kryo kryo = client.getKryo();
            kryo.register(String.class);
            kryo.register(byte[].class);
            kryo.register(TranslateObject.class);
            kryo.register(HashCode.class);
            kryo.register(ZoomObject.class);
            kryo.register(BytePart.class);
            kryo.register(RectObject.class);
            kryo.register(Double[].class);
            client.addListener(new Listener() {
                public void received (Connection connection, Object object) {

                    if (object instanceof BytePart)
                    {
                        //System.out.print("byte,");
                        BytePart part = (BytePart)object;
                        arrayList.add(part.part);
                    }
                    if(object instanceof HashCode)
                    {
                        HashCode hashCode = (HashCode)object;
                        if(hasImage(hashCode.hash))
                        {
                            hashCode.clientAlreadyHasImage = true;
                            client.sendTCP(hashCode);
                            setImage(hashCode.hash);
                        }
                        else
                        {
                            hashCode.clientAlreadyHasImage = false;
                            newUUID = hashCode.hash;
                            client.sendTCP(hashCode);
                        }
                    }
                    if(object instanceof TranslateObject)
                    {
                        TranslateObject translateObject = (TranslateObject)object;
                        if(currentImage != null)
                        {
                            currentImage.setX(translateObject.x);
                            currentImage.setY(translateObject.y);

                        }
                        translate(translateObject.x, translateObject.y);
                    }
                    if(object instanceof ZoomObject)
                    {
                        ZoomObject translateObject = (ZoomObject)object;
                        if(currentImage != null)
                        {
                            currentImage.setScaleX(translateObject.scaleX);
                            currentImage.setScaleY(translateObject.scaleY);

                        }
                        zoom(translateObject.scaleX, translateObject.scaleY);
                    }
                    if (object instanceof RectObject) {
                        RectObject obj = (RectObject) object;
                        if (currentImage != null) {
                            Polygon rect = new Polygon();
                            rect.getPoints().addAll(obj.array);
                            currentImage.addRectangle(rect);
                            subtract(rect);
                        }

                    }
                    if (object instanceof String)
                    {
                        //System.out.println("--");
                        //System.out.println(object.toString());
                        if(object.equals("start"))
                        {
                            arrayList = new ArrayList<>();
                        }
                        else
                        {
                            System.out.println("Stop");
                            ArrayList<Byte> byteArray = new ArrayList<>();

                            for(byte[] array : arrayList)
                            {
                                for(byte by : array)
                                {
                                    byteArray.add(by);
                                }
                            }


                            byte[] bytes = new byte[byteArray.size()];
                            for(int i = 0; i< byteArray.size(); i++)
                            {
                                bytes[i] = byteArray.get(i);
                            }

                            try {
                                BufferedImage img = ImageIO.read(new ByteArrayInputStream(bytes));
                                newImage(SwingFXUtils.toFXImage(img, null));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    private void subtract(Polygon rect) {

        // Notify everybody that may be interested.
        for (ImageListener hl : listeners)
            hl.subtract(rect);
    }

    private void setImage(int hashcode) {
        for(ImageSelection image : imageSelections)
        {
            if(image.getHash() == hashcode)
            {
                currentImage = image;
                setImage(image);
                translate(image.getX(), image.getY());
                zoom(image.getScaleX(), image.getScaleY());
            }
        }
    }

    public boolean hasImage(int hashcode)
    {
        System.out.println("new: "+  hashcode);
        for(ImageSelection image : imageSelections)
        {
            System.out.println("-: "+  image.getHash());
            if(image.getHash() == hashcode)
            {
                return true;
            }
        }
        return false;
    }

    public void translate(double x, double y) {


        // Notify everybody that may be interested.
        for (ImageListener hl : listeners)
            hl.translate(x, y);
    }

    public void zoom(double x, double y) {


        // Notify everybody that may be interested.
        for (ImageListener hl : listeners)
            hl.zoom(x, y);
    }


    public void setImage(ImageSelection image)
    {
        for (ImageListener hl : listeners) {
            hl.newImageSet(image);
        }
    }

    public void newImage(Image image)
    {
        if(newUUID >= 0) {
            System.out.println("new image ");
            ImageSelection imageSelection = new ImageSelection(image, "");
            imageSelection.setX(0);
            imageSelection.setY(0);
            imageSelection.setScaleX(1);
            imageSelection.setScaleY(1);
            imageSelection.setHash(newUUID);
            currentImage = imageSelection;
            imageSelections.add(imageSelection);

            setImage(imageSelection);
        }
    }


    public void addListener(ImageListener toAdd) {
        listeners.add(toAdd);
    }
}
