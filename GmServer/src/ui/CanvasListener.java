package ui;

/**
 * Created by Michel on 23-3-2015.
 */
public interface CanvasListener
{
    public void translate(double x, double y);

    public void zoom(double x, double y);

    public void subtract(Double[] array);
}
