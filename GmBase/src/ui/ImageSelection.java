package ui;

import javafx.scene.image.Image;
import javafx.scene.shape.Polygon;

import java.util.ArrayList;

/**
 * Created by Michel on 22-3-2015.
 */
public class ImageSelection
{
    private int hash;
    private Image image;
    private String path;
    private double x;
    private double y;
    private double scaleX;
    private double scaleY;
    private ArrayList<Polygon> rectangleArrayList = new ArrayList<Polygon>();

    public ImageSelection(Image thumbprint, String path) {
        this.image = thumbprint;
        this.path = path;
    }

    public double getScaleX() {
        return scaleX;
    }

    public void setScaleX(double scaleX) {
        this.scaleX = scaleX;
    }

    public double getScaleY() {
        return scaleY;
    }

    public void setScaleY(double scaleY) {
        this.scaleY = scaleY;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public ArrayList<Polygon> getRectangleArrayList() {
        return rectangleArrayList;
    }

    public void setRectangleArrayList(ArrayList<Polygon> rectangleArrayList) {
        this.rectangleArrayList = rectangleArrayList;
    }

    public void addRectangle(Polygon rect) {
        rectangleArrayList.add(rect);
    }
}
