package ui;

import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.ZoomEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Path;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

/**
 * Created by Michel on 23-3-2015.
 */
public class ClientCanvas extends StackPane
{
    List<CanvasListener> listeners = new ArrayList<>();
    ImageSelection selection;
    private Timer timer = new Timer();
    private double pressedX, pressedY;
    private Shape fow;
    private Pane layout;
    private Polygon drag = new Polygon();
    private boolean showPoly = false;

    public ClientCanvas(ImageSelection imageObj)
    {

        this.selection = imageObj;
        setTranslateX(selection.getX());
        setTranslateY(selection.getY());
        ImageView view = new ImageView();
        view.setImage(selection.getImage());
        setMinSize(600, 600);
        setStyle("-fx-border-color: blue;");
        layout = new Pane();
        fow = new Rectangle(selection.getImage().getWidth(), selection.getImage().getHeight());
        fow.setFill(Color.rgb(0, 0, 0, 0.5));

        fow.setStrokeWidth(1);
        fow.setStroke(Color.BLACK);

        for (Polygon rect : imageObj.getRectangleArrayList()) {
            fow = Path.subtract(fow, rect);
        }

        layout.getChildren().setAll(
                view,
                fow
        );


        getChildren().add(layout);

        setOnMousePressed(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                pressedX = event.getX();
                pressedY = event.getY();
                if (event.isPrimaryButtonDown()) {
                    drag.getPoints().addAll(pressedX, pressedY);
                    drag.setFill(Color.rgb(255, 255, 255, 0.5));
                    if (!layout.getChildren().contains(drag)) {
                        layout.getChildren().add(drag);
                    }

                }
                if (event.isSecondaryButtonDown()) {

                    drag.getPoints().addAll(pressedX, pressedY);
                    if (drag.getPoints().size() >= 3) {
                        System.out.println(drag.getPoints().size());

                        Polygon rect = new Polygon();
                        rect.getPoints().addAll(drag.getPoints());
                        ArrayList<Double> points = new ArrayList<Double>(drag.getPoints());

                        selection.addRectangle(rect);
                        layout.getChildren().clear();
                        layout.getChildren().setAll(view, fow);
                        fow = new Rectangle(selection.getImage().getWidth(), selection.getImage().getHeight());

                        for (Polygon poly : imageObj.getRectangleArrayList()) {
                            fow = Path.subtract(fow, poly);
                        }

                        //fow.setScaleX(getScaleX());
                        //fow.setScaleY(getScaleY());
                        subtract(points.toArray(new Double[points.size()]));
                        layout.getChildren().clear();

                        fow.setFill(Color.rgb(0, 0, 0, 0.5));
                        layout.getChildren().clear();
                        layout.getChildren().setAll(view, fow);
                    }
                    drag = new Polygon();

                }
            }
        });


        setOnMouseDragged(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                if (event.isMiddleButtonDown()) {

                    setTranslateX(getTranslateX() + (getScaleX() * (event.getX() - pressedX)));
                    setTranslateY(getTranslateY() + (getScaleY() * (event.getY() - pressedY)));
                    selection.setX(getTranslateX() + (getScaleX() * (event.getX() - pressedX)));
                    selection.setY(getTranslateY() + (getScaleY() * (event.getY() - pressedY)));
                    translate(getTranslateX() + (getScaleX() * (event.getX() - pressedX)), getTranslateY() + (getScaleY() * (event.getY() - pressedY)));
                    event.consume();
                }
            }
        });


        setOnScroll(new EventHandler<ScrollEvent>() {
            @Override
            public void handle(ScrollEvent event) {
                double zoomFactor = 1.05;
                double deltaY = event.getDeltaY();
                if (deltaY < 0){
                    zoomFactor = 2.0 - zoomFactor;
                }


                setScaleX(getScaleX() * zoomFactor);
                setScaleY(getScaleY() * zoomFactor);
                selection.setScaleX(getScaleX());
                selection.setScaleY(getScaleY());
                zoom(getScaleX(), getScaleY());
                System.out.println(getScaleX());
                System.out.println(getScaleY());
                event.consume();
        }
        });
        setOnZoom(new EventHandler<ZoomEvent>() {
            @Override
            public void handle(ZoomEvent event) {
                System.out.println(event.getZoomFactor());
            }
        });
    }


    public void translate(double x, double y)
    {
        for (CanvasListener hl : listeners)
            hl.translate(x,y);
    }

    public void subtract(Double[] array) {
        for (CanvasListener hl : listeners)
            hl.subtract(array);
    }


    public void zoom(double x, double y)
    {
        for (CanvasListener hl : listeners)
            hl.zoom(x, y);
    }

    public void addListener(CanvasListener toAdd) {
        listeners.add(toAdd);
    }

    public void removeListener(Main main) {
        listeners.remove(main);
    }
}
