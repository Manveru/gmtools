package ui.notes;

import controllers.NoteController;
import data.notes.Entry;
import data.notes.Item;
import data.notes.NoteTypeEnum;
import data.notes.Project;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.util.StringConverter;

/**
 * Created by Michel on 29-3-2015.
 */
public class NoteBookPane extends BorderPane {

    TreeItem<Entry> rootNode;

    NoteController controller;


    public NoteBookPane(NoteController controller) {
        this.controller = controller;

        Entry entry = new Entry();
        entry.setTitel("Create or open a project");
        addTreeMenu(entry);
    }

    private void addTreeMenu(Entry main) {

        rootNode = new TreeItem<>(main);
        rootNode.setExpanded(true);

        for (Entry entry : main.getSubEntries()) {
            TreeItem<Entry> entryLeaf = new TreeItem<>(entry);
            addChilderen(entryLeaf, entry);
            rootNode.getChildren().add(entryLeaf);

            }
        TreeView<Entry> treeView = new TreeView<>(rootNode);
        treeView.setEditable(true);
        treeView.setCellFactory(p -> new TextFieldTreeCellImpl());
        setLeft(treeView);


        ListView<Item> items = new ListView<>();

        treeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                Entry entry = newValue.getValue();
                if (entry != null && (entry.getType() != NoteTypeEnum.CATEGORY && entry.getType() != NoteTypeEnum.TEMP)) {
                    ObservableList<Item> myObservableList = FXCollections.observableList(entry.getItems());
                    items.setItems(myObservableList);
                }
            }
        });
        items.setEditable(true);


        ContextMenu menu = new ContextMenu();
        MenuItem addMenuItem = new MenuItem("Add entry");
        MenuItem addcatItem = new MenuItem("Add Category");
        MenuItem remItemItem = new MenuItem("Remove item");
        menu.getItems().addAll(addMenuItem, addcatItem, remItemItem);
        addMenuItem.setOnAction(t -> {

            System.out.println("On action");
            TreeItem<Entry> entry = treeView.getSelectionModel().selectedItemProperty().getValue();
            if (entry != null && entry.getValue() != null) {
                items.getItems().clear();
                Item item = new Item();
                item.setContent("New item");
                entry.getValue().addItem(item);

                ObservableList<Item> myObservableList = FXCollections.observableList(entry.getValue().getItems());

                items.setItems(myObservableList);
            }


        });


        items.setContextMenu(menu);
        items.setCellFactory(p -> new TextFieldListCell<Item>(new StringConverter<Item>() {
            @Override
            public String toString(Item object) {
                if (object != null) {
                    return object.getContent();
                } else {
                    return "";
                }

            }

            @Override
            public Item fromString(String string) {
                return null;
            }
        }));
        setCenter(items);
    }

    private void addChilderen(TreeItem<Entry> current, Entry currentEntry) {
        for (Entry entry : currentEntry.getSubEntries())
        {
            TreeItem<Entry> entryLeaf = new TreeItem<>(entry);
            addChilderen(entryLeaf, entry);

            current.getChildren().add(entryLeaf);

        }
    }

    public void setCurrentProject(Project project) {
        if (project == null) {
            Entry entry = new Entry();
            entry.setTitel("Create or open a project");
            entry.setType(NoteTypeEnum.TEMP);
            addTreeMenu((entry));
        } else {

            System.out.println("Entry" + project.getEntry());
            addTreeMenu(project.getEntry());
        }
    }

    // Listview template


    private final class TextFieldListCellImpl extends ListCell<Item> {

        private TextField textField;
        private ContextMenu addMenu = new ContextMenu();

        public TextFieldListCellImpl() {

        }

        @Override
        public void startEdit() {
            Item item = this.getItem();
            super.startEdit();

            if (textField == null) {
                createTextField(item);
            }
            setText(null);
            setGraphic(textField);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(getItem().getContent());

        }

        @Override
        public void updateItem(Item item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setText(null);
                    setGraphic(textField);
                } else {
                    setText(getString());

                    setContextMenu(addMenu);

                }
            }
        }

        private void createTextField(Item item) {
            textField = new TextField(getString());
            textField.setOnKeyReleased(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        item.setContent(textField.getText());
                        commitEdit(item);
                    } else if (t.getCode() == KeyCode.ESCAPE) {

                        cancelEdit();
                    }
                }
            });

        }

        private String getString() {
            return getItem() == null ? "" : getItem().getContent();
        }
    }


    // Treeview template
    private final class TextFieldTreeCellImpl extends TreeCell<Entry> {

        private TextField textField;
        private ContextMenu addMenu = new ContextMenu();

        public TextFieldTreeCellImpl() {

            MenuItem addMenuItem = new MenuItem("Add entry");
            MenuItem addcatItem = new MenuItem("Add Category");
            MenuItem remItemItem = new MenuItem("Remove item");
            addMenu.getItems().addAll(addMenuItem, addcatItem, remItemItem);
            addMenuItem.setOnAction(t -> {

                System.out.println("On action");
                Entry entry = this.getItem();

                Entry subentry = new Entry();
                subentry.setTitel("New Entry");
                subentry.setType(NoteTypeEnum.NOTE);


                entry.addSubEntry(subentry);
                TreeItem newEmployee = new TreeItem(subentry);
                getTreeItem().getChildren().add(newEmployee);
            });

            addcatItem.setOnAction(t -> {
                Entry entry = this.getItem();

                Entry subentry = new Entry();
                subentry.setTitel("New Entry");
                subentry.setType(NoteTypeEnum.CATEGORY);


                entry.addSubEntry(subentry);
                TreeItem newEmployee = new TreeItem(subentry);
                getTreeItem().getChildren().add(newEmployee);
            });

            remItemItem.setOnAction(t -> {
                TreeItem item = getTreeItem();
                if (item.getParent() != null) {
                    boolean remove = getTreeItem().getParent().getChildren().remove(item);
                }
            });
        }

        @Override
        public void startEdit() {
            Entry entry = this.getItem();
            super.startEdit();

            if (textField == null) {
                createTextField(entry);
            }
            setText(null);
            setGraphic(textField);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(getItem().getTitel());
            setGraphic(getTreeItem().getGraphic());
        }

        @Override
        public void updateItem(Entry item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setText(null);
                    setGraphic(textField);
                } else {
                    setText(getString());
                    setGraphic(getTreeItem().getGraphic());
                    if (getTreeItem().getValue().getType() != NoteTypeEnum.TEMP) {
                        setContextMenu(addMenu);
                    }
                }
            }
        }

        private void createTextField(Entry entry) {
            textField = new TextField(getString());
            textField.setOnKeyReleased(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        entry.setTitel(textField.getText());
                        commitEdit(entry);
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });

        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }
}