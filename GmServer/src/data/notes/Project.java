package data.notes;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Michel on 1-4-2015.
 */
public class Project implements Serializable {
    private String titel;
    private Entry mainEntry = new Entry();
    private ArrayList<String> categories = new ArrayList<>();

    public Project(String titel) {
        this.titel = titel;
    }

    public Entry getEntry() {
        return mainEntry;
    }

    public void setEntry(Entry mainEntry) {
        this.mainEntry = mainEntry;
    }


    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }


    public void addCategory(String category) {
        categories.add(category);
    }

    public ArrayList<String> getCategories() {
        return categories;
    }
}
