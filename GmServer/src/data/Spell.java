package data;

import org.apache.commons.csv.CSVRecord;

/**
 * Created by Michel on 29-3-2015.
 */
public class Spell extends PathfinderObject
{
    private String name;
    private String school;
    private String subschool;
    private String descriptor;
    private String spell_level;
    private String casting_time;
    private String components;
    private String costly_components;
    private String range;
    private String area;
    private String effect;
    private String targets;
    private String duration;
    private String dismissible;
    private String shapeable;
    private String saving_throw;
    private String spell_resistence;
    private String description;
    private String description_formated;
    private String source;
    private String full_text;
    private String verbal;
    private String somatic;
    private String material;
    private String focus;
    private String divine_focus;
    private String sor;
    private String wiz;
    private String cleric;
    private String druid;
    private String ranger;
    private String bard;
    private String paladin;
    private String alchemist;
    private String summoner;
    private String witch;
    private String inquisitor;
    private String oracle;
    private String antipaladin;
    private String magus;
    private String adept;
    private String deity;
    private String SLA_Level;
    private String domain;
    private String short_description;
    private String acid;
    private String air;
    private String chaotic;
    private String cold;
    private String curse;
    private String darkness;
    private String death;
    private String disease;
    private String earth;
    private String electricity;
    private String emotion;
    private String evil;
    private String fear;
    private String fire;
    private String force;
    private String good;
    private String language_dependent;
    private String lawful;
    private String light;
    private String mind_affecting;
    private String pain;
    private String poison;
    private String shadow;
    private String sonic;
    private String water;
    private String linktext;
    private String material_costs;
    private String bloodline;
    private String patron;
    private String mythic_text;
    private String augmented;
    private String mythic;
    private String bloodrager;
    private String shaman;

    @Override
    public void parseObject(CSVRecord record) {
        setName(record.get("name"));
        setSchool(record.get("school"));
        setSubschool(record.get("subschool"));
        setDescriptor(record.get("descriptor"));
        setSpell_level(record.get("spell_level"));
        setCasting_time(record.get("casting_time"));
        setComponents(record.get("components"));
        setCostly_components(record.get("costly_components"));
        setRange(record.get("range"));
        setArea(record.get("area"));
        setEffect(record.get("effect"));
        setTargets(record.get("targets"));
        setDuration(record.get("duration"));
        setDismissible(record.get("dismissible"));
        setShapeable(record.get("shapeable"));
        setSaving_throw(record.get("saving_throw"));
        setSpell_resistence(record.get("spell_resistence"));
        setDescription(record.get("description"));
        setDescription_formated(record.get("description_formated"));
        setSource(record.get("source"));
        setFull_text(record.get("full_text"));
        setVerbal(record.get("verbal"));
        setSomatic(record.get("somatic"));
        setMaterial(record.get("material"));
        setFocus(record.get("focus"));
        setDivine_focus(record.get("divine_focus"));
        setSor(record.get("sor"));
        setWiz(record.get("wiz"));
        setCleric(record.get("cleric"));
        setDruid(record.get("druid"));
        setRanger(record.get("ranger"));
        setBard(record.get("bard"));
        setPaladin(record.get("paladin"));
        setAlchemist(record.get("alchemist"));
        setSummoner(record.get("summoner"));
        setWitch(record.get("witch"));
        setInquisitor(record.get("inquisitor"));
        setOracle(record.get("oracle"));
        setAntipaladin(record.get("antipaladin"));
        setMagus(record.get("magus"));
        setAdept(record.get("adept"));
        setDeity(record.get("deity"));
        setSLA_Level(record.get("SLA_Level"));
        setDomain(record.get("domain"));
        setShort_description(record.get("short_description"));
        setAcid(record.get("acid"));
        setAir(record.get("air"));
        setChaotic(record.get("chaotic"));
        setCold(record.get("cold"));
        setCurse(record.get("curse"));
        setDarkness(record.get("darkness"));
        setDeath(record.get("death"));
        setDisease(record.get("disease"));
        setEarth(record.get("earth"));
        setElectricity(record.get("electricity"));
        setEmotion(record.get("emotion"));
        setEvil(record.get("evil"));
        setFear(record.get("fear"));
        setFire(record.get("fire"));
        setForce(record.get("force"));
        setGood(record.get("good"));
        setLanguage_dependent(record.get("language_dependent"));
        setLawful(record.get("lawful"));
        setLight(record.get("light"));
        setMind_affecting(record.get("mind_affecting"));
        setPain(record.get("pain"));
        setPoison(record.get("poison"));
        setShadow(record.get("shadow"));
        setSonic(record.get("sonic"));
        setWater(record.get("water"));
        setLinktext(record.get("linktext"));
        setId(Integer.parseInt(record.get("id")));
        setMaterial_costs(record.get("material_costs"));
        setBloodline(record.get("bloodline"));
        setPatron(record.get("patron"));
        setMythic_text(record.get("mythic_text"));
        setAugmented(record.get("augmented"));
        setMythic(record.get("mythic"));
        setBloodrager(record.get("bloodrager"));
        setShaman(record.get("shaman"));

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getSubschool() {
        return subschool;
    }

    public void setSubschool(String subschool) {
        this.subschool = subschool;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }

    public String getSpell_level() {
        return spell_level;
    }

    public void setSpell_level(String spell_level) {
        this.spell_level = spell_level;
    }

    public String getCasting_time() {
        return casting_time;
    }

    public void setCasting_time(String casting_time) {
        this.casting_time = casting_time;
    }

    public String getComponents() {
        return components;
    }

    public void setComponents(String components) {
        this.components = components;
    }

    public String getCostly_components() {
        return costly_components;
    }

    public void setCostly_components(String costly_components) {
        this.costly_components = costly_components;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public String getTargets() {
        return targets;
    }

    public void setTargets(String targets) {
        this.targets = targets;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDismissible() {
        return dismissible;
    }

    public void setDismissible(String dismissible) {
        this.dismissible = dismissible;
    }

    public String getShapeable() {
        return shapeable;
    }

    public void setShapeable(String shapeable) {
        this.shapeable = shapeable;
    }

    public String getSaving_throw() {
        return saving_throw;
    }

    public void setSaving_throw(String saving_throw) {
        this.saving_throw = saving_throw;
    }

    public String getSpell_resistence() {
        return spell_resistence;
    }

    public void setSpell_resistence(String spell_resistence) {
        this.spell_resistence = spell_resistence;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription_formated() {
        return description_formated;
    }

    public void setDescription_formated(String description_formated) {
        this.description_formated = description_formated;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getFull_text() {
        return full_text;
    }

    public void setFull_text(String full_text) {
        this.full_text = full_text;
    }

    public String getVerbal() {
        return verbal;
    }

    public void setVerbal(String verbal) {
        this.verbal = verbal;
    }

    public String getSomatic() {
        return somatic;
    }

    public void setSomatic(String somatic) {
        this.somatic = somatic;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getFocus() {
        return focus;
    }

    public void setFocus(String focus) {
        this.focus = focus;
    }

    public String getDivine_focus() {
        return divine_focus;
    }

    public void setDivine_focus(String divine_focus) {
        this.divine_focus = divine_focus;
    }

    public String getSor() {
        return sor;
    }

    public void setSor(String sor) {
        this.sor = sor;
    }

    public String getWiz() {
        return wiz;
    }

    public void setWiz(String wiz) {
        this.wiz = wiz;
    }

    public String getCleric() {
        return cleric;
    }

    public void setCleric(String cleric) {
        this.cleric = cleric;
    }

    public String getDruid() {
        return druid;
    }

    public void setDruid(String druid) {
        this.druid = druid;
    }

    public String getRanger() {
        return ranger;
    }

    public void setRanger(String ranger) {
        this.ranger = ranger;
    }

    public String getBard() {
        return bard;
    }

    public void setBard(String bard) {
        this.bard = bard;
    }

    public String getPaladin() {
        return paladin;
    }

    public void setPaladin(String paladin) {
        this.paladin = paladin;
    }

    public String getAlchemist() {
        return alchemist;
    }

    public void setAlchemist(String alchemist) {
        this.alchemist = alchemist;
    }

    public String getSummoner() {
        return summoner;
    }

    public void setSummoner(String summoner) {
        this.summoner = summoner;
    }

    public String getWitch() {
        return witch;
    }

    public void setWitch(String witch) {
        this.witch = witch;
    }

    public String getInquisitor() {
        return inquisitor;
    }

    public void setInquisitor(String inquisitor) {
        this.inquisitor = inquisitor;
    }

    public String getOracle() {
        return oracle;
    }

    public void setOracle(String oracle) {
        this.oracle = oracle;
    }

    public String getAntipaladin() {
        return antipaladin;
    }

    public void setAntipaladin(String antipaladin) {
        this.antipaladin = antipaladin;
    }

    public String getMagus() {
        return magus;
    }

    public void setMagus(String magus) {
        this.magus = magus;
    }

    public String getAdept() {
        return adept;
    }

    public void setAdept(String adept) {
        this.adept = adept;
    }

    public String getDeity() {
        return deity;
    }

    public void setDeity(String deity) {
        this.deity = deity;
    }

    public String getSLA_Level() {
        return SLA_Level;
    }

    public void setSLA_Level(String SLA_Level) {
        this.SLA_Level = SLA_Level;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public String getAcid() {
        return acid;
    }

    public void setAcid(String acid) {
        this.acid = acid;
    }

    public String getAir() {
        return air;
    }

    public void setAir(String air) {
        this.air = air;
    }

    public String getChaotic() {
        return chaotic;
    }

    public void setChaotic(String chaotic) {
        this.chaotic = chaotic;
    }

    public String getCold() {
        return cold;
    }

    public void setCold(String cold) {
        this.cold = cold;
    }

    public String getCurse() {
        return curse;
    }

    public void setCurse(String curse) {
        this.curse = curse;
    }

    public String getDarkness() {
        return darkness;
    }

    public void setDarkness(String darkness) {
        this.darkness = darkness;
    }

    public String getDeath() {
        return death;
    }

    public void setDeath(String death) {
        this.death = death;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getEarth() {
        return earth;
    }

    public void setEarth(String earth) {
        this.earth = earth;
    }

    public String getElectricity() {
        return electricity;
    }

    public void setElectricity(String electricity) {
        this.electricity = electricity;
    }

    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    public String getEvil() {
        return evil;
    }

    public void setEvil(String evil) {
        this.evil = evil;
    }

    public String getFear() {
        return fear;
    }

    public void setFear(String fear) {
        this.fear = fear;
    }

    public String getFire() {
        return fire;
    }

    public void setFire(String fire) {
        this.fire = fire;
    }

    public String getForce() {
        return force;
    }

    public void setForce(String force) {
        this.force = force;
    }

    public String getGood() {
        return good;
    }

    public void setGood(String good) {
        this.good = good;
    }

    public String getLanguage_dependent() {
        return language_dependent;
    }

    public void setLanguage_dependent(String language_dependent) {
        this.language_dependent = language_dependent;
    }

    public String getLawful() {
        return lawful;
    }

    public void setLawful(String lawful) {
        this.lawful = lawful;
    }

    public String getLight() {
        return light;
    }

    public void setLight(String light) {
        this.light = light;
    }

    public String getMind_affecting() {
        return mind_affecting;
    }

    public void setMind_affecting(String mind_affecting) {
        this.mind_affecting = mind_affecting;
    }

    public String getPain() {
        return pain;
    }

    public void setPain(String pain) {
        this.pain = pain;
    }

    public String getPoison() {
        return poison;
    }

    public void setPoison(String poison) {
        this.poison = poison;
    }

    public String getShadow() {
        return shadow;
    }

    public void setShadow(String shadow) {
        this.shadow = shadow;
    }

    public String getSonic() {
        return sonic;
    }

    public void setSonic(String sonic) {
        this.sonic = sonic;
    }

    public String getWater() {
        return water;
    }

    public void setWater(String water) {
        this.water = water;
    }

    public String getLinktext() {
        return linktext;
    }

    public void setLinktext(String linktext) {
        this.linktext = linktext;
    }

    public String getMaterial_costs() {
        return material_costs;
    }

    public void setMaterial_costs(String material_costs) {
        this.material_costs = material_costs;
    }

    public String getBloodline() {
        return bloodline;
    }

    public void setBloodline(String bloodline) {
        this.bloodline = bloodline;
    }

    public String getPatron() {
        return patron;
    }

    public void setPatron(String patron) {
        this.patron = patron;
    }

    public String getMythic_text() {
        return mythic_text;
    }

    public void setMythic_text(String mythic_text) {
        this.mythic_text = mythic_text;
    }

    public String getAugmented() {
        return augmented;
    }

    public void setAugmented(String augmented) {
        this.augmented = augmented;
    }

    public String getMythic() {
        return mythic;
    }

    public void setMythic(String mythic) {
        this.mythic = mythic;
    }

    public String getBloodrager() {
        return bloodrager;
    }

    public void setBloodrager(String bloodrager) {
        this.bloodrager = bloodrager;
    }

    public String getShaman() {
        return shaman;
    }

    public void setShaman(String shaman) {
        this.shaman = shaman;
    }


}
