package data;

import org.apache.commons.csv.CSVRecord;

/**
 * Created by Michel on 28-3-2015.
 */
public abstract class PathfinderObject {

    protected int id = 0;

    public void parseObject(CSVRecord record) {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
