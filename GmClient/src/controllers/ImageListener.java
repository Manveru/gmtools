package controllers;

import javafx.scene.shape.Polygon;
import ui.ImageSelection;

/**
 * Created by Michel on 22-3-2015.
 */
public interface ImageListener
{
    public void newImageSet(ImageSelection image);
    public void translate(double x, double y);
    public void zoom(double x, double y);

    public void subtract(Polygon rectangle);
}
