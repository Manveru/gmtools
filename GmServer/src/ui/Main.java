package ui;

import controllers.*;
import data.*;
import data.notes.Entry;
import data.notes.NoteTypeEnum;
import data.notes.Project;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ui.Encounters.RandomEncounterPane;
import ui.datapanes.DataFeatPane;
import ui.datapanes.DataItemsPane;
import ui.datapanes.DataNpcPane;
import ui.datapanes.DataSpellPane;
import ui.notes.NoteBookPane;

import java.io.File;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Sample application that shows examples of the different layout panes
 * provided by the JavaFX layout API.
 * The resulting UI is for demonstration purposes only and is not interactive.
 */
public class Main extends Application implements CanvasListener {


    private ArrayList<PcgFile> pcgFiles = null;
    private BorderPane border;
    private ArrayList<ImageSelection> images;
    private ArrayList<NPC> NPCs;
    private ArrayList<Monster> monsters;
    private ArrayList<MagicItem> items;
    private ArrayList<Feat> feats;
    private ArrayList<Spell> spells;
    private ArrayList<Entry> notes;
    private Controller controller;
    private PcGenController pcGenController;
    private ImageSelection currentImage;
    private CsvController csvController;
    private NoteController noteController;
    private NoteBookPane notePane;
    private MenuBar menuBar;
    private Project currentProject;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(Main.class, args);
    }

    @Override
    public void start(Stage stage) {

        // Use a border notePane as the root for scene
        csvController = new CsvController();
        NPCs = csvController.parseNpcCsv();
        //monsters = csvController.parseMonsterCsv();
        feats = csvController.parseFeatsCsv();
        items = csvController.parseMagicItemCsv();
        spells = csvController.parseSpellsCsv();
        menuBar = new MenuBar();
        noteController = new NoteController();
        notePane = new NoteBookPane(noteController);

        // </notes test>




        border = new BorderPane();
        images = new ArrayList<>();
        controller = new Controller();
        controller.readCharacters();

        pcGenController = new PcGenController();
        pcgFiles = new ArrayList<>();
        addMenu();

        border.setCenter(addGridPane());

        Scene scene = new Scene(border);
        stage.setScene(scene);
        stage.setTitle("Pathfinder GM tools");
        border.setPrefSize(800, 600);
        stage.show();
    }

    /*
     * Creates a VBox with a list of links for the left region
     */
    private VBox addCStartVBox() {

        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10)); // Set all sides to 10
        vbox.setSpacing(8);              // Gap between nodes

        Text title = new Text("Menu");
        title.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        vbox.getChildren().add(title);

        Hyperlink options[] = new Hyperlink[]{
                new Hyperlink("Menu 1"),
                new Hyperlink("Menu 2"),
                new Hyperlink("Menu 3"),
                new Hyperlink("Menu 4")};

        for (int i = 0; i < 4; i++) {
            // Add offset to left side to indent from title
            VBox.setMargin(options[i], new Insets(0, 0, 0, 8));
            vbox.getChildren().add(options[i]);
        }

        return vbox;
    }

    /*
     * Creates a grid for the center region with four columns and three rows
     */
    private GridPane addGridPane() {

        GridPane grid = new GridPane();
        grid.autosize();

        grid.setHgap(10);
        grid.setVgap(10);
        //grid.setGridLinesVisible(true);
        grid.setPadding(new Insets(0, 10, 0, 10));
        grid.setAlignment(Pos.TOP_CENTER);

        ColumnConstraints column1 = new ColumnConstraints(20);
        ColumnConstraints column2 = new ColumnConstraints(100,100,150);
        ColumnConstraints column3 = new ColumnConstraints(100,100,Double.MAX_VALUE);
        column2.setHgrow(Priority.ALWAYS);
        column3.setHgrow(Priority.ALWAYS);
        grid.getColumnConstraints().addAll(column1, column2,column3); // first column gets any extra width

        RowConstraints row1 = new RowConstraints(20, 20, 20);
        RowConstraints row2 = new RowConstraints(50, 50, Double.MAX_VALUE);
        row2.setVgrow(Priority.ALWAYS);

        grid.getRowConstraints().addAll(row1, row1, row1, row1, row1, row1, row1, row1, row2, row1, row1, row2);

        // Category in column 2, row 1
        Text category = new Text("Server settings:");
        category.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(category, 1, 0);


        // Subtitle in columns 2-3, row 2
        Button startServer = new Button("Start server");
        startServer.setOnAction(e -> controller.StartServer());



        grid.add(startServer, 1, 1);

        Button stopServer = new Button("Stop server");
        stopServer.setOnAction(e -> controller.stopServer());

        grid.add(stopServer, 2, 1);

        Text pcgenFileslabel = new Text("Player character files:");
        pcgenFileslabel.setFont(Font.font("Arial", FontWeight.BOLD, 13));
        grid.add(pcgenFileslabel, 1, 4, 2, 1);

        TextField pcgenFilename = new TextField("C:\\Users\\Michel\\Dropbox\\Shared\\Rise of the runelords");
        pcgenFilename.setFont(Font.font("Arial", FontWeight.BOLD, 13));
        grid.add(pcgenFilename, 2, 4, 4, 1);

        Button loadCharacters = new Button("Load characters");
        loadCharacters.setOnAction(e -> pcgFiles = pcGenController.getTabData(pcgenFilename.getText()));

        grid.add(loadCharacters, 1, 5);


        // Category in column 2, row 1
        Text drop = new Text("Drop images:");
        drop.setFont(Font.font("Arial", FontWeight.NORMAL, 13));
        grid.add(drop, 1, 7, 2, 1);


        // Chart in columns 2-3, row 3
        AnchorPane canvas = new AnchorPane();
        canvas.setStyle("-fx-border-color: black;");
        canvas.setMinSize(200, 200);
        //canvas.setPrefSize(200,200);

        canvas.setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                if (db.hasFiles()) {
                    event.acceptTransferModes(TransferMode.COPY);
                } else {
                    event.consume();
                }
            }
        });

        canvas.setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasFiles()) {
                    success = true;
                    String filePath;
                    for (File file : db.getFiles()) {
                        filePath = file.getAbsolutePath();
                        ImageSelection img = controller.getImage(filePath);
                        if (img != null) {
                            images.add(img);
                            border.setCenter(addGridPane());
                        }
                    }
                }
                event.setDropCompleted(success);
                event.consume();
            }
        });

        grid.add(canvas, 1, 7, 2, 2);

        Button btn = new Button();
        btn.setText("Open client preview");
        btn.setOnAction(event -> {

            if (currentImage != null) {

                ImageView view = new ImageView();
                view.setImage(currentImage.getImage());
                ClientCanvas canvas1 = new ClientCanvas(currentImage);

                canvas1.addListener(getMain());
                Scene secondScene = new Scene(canvas1, 800, 600);

                Stage secondStage = new Stage();
                secondStage.setTitle("Client preview");
                secondStage.setScene(secondScene);
                secondStage.show();
                secondStage.setOnCloseRequest(we -> canvas1.removeListener(getMain()));
            }
        });

        grid.add(btn, 1, 9, 1, 1);

        grid.add(addImageFlowPane(), 1, 11, 2, 2);

//        grid.setGridLinesVisible(true);
        return grid;
    }

    /*
   * Creates a grid for the center region with four columns and three rows
   */
    private BorderPane addCharacterPane() {

        TabPane tabPane = new TabPane();

        BorderPane borderPane = new BorderPane();
        for (PcgFile pcgFile : pcgFiles) {
            Tab tab = new Tab();
            tab.setText(pcgFile.getName());
            Accordion accordion = new Accordion();
            TextArea bioText = new TextArea(pcgFile.getBio());
            bioText.setWrapText(true);
            TitledPane t1 = new TitledPane("Bio", bioText);
            TextArea descArea = new TextArea(pcgFile.getDescription());
            descArea.setWrapText(true);
            TitledPane t2 = new TitledPane("Description", descArea);
            accordion.getPanes().addAll(t1, t2);

            tab.setContent(accordion);
            tabPane.getTabs().add(tab);
        }
        borderPane.setCenter(tabPane);
        return borderPane;
    }

    /*
   * Creates a grid for the center region with four columns and three rows
   */
    private TabPane addDataPane() {
        TabPane tabPane = new TabPane();

        Tab tabNpcs = new Tab("NPCs");
        tabNpcs.setClosable(false);
        DataNpcPane npcPane = new DataNpcPane(NPCs);
        tabNpcs.setContent(npcPane);
        tabPane.getTabs().add(tabNpcs);


        Tab tabFeats = new Tab("Feats");
        tabFeats.setClosable(false);
        DataFeatPane featsPane = new DataFeatPane(feats);
        tabFeats.setContent(featsPane);
        tabPane.getTabs().add(tabFeats);

        Tab tabitems = new Tab("Magic items");
        tabitems.setClosable(false);
        DataItemsPane itemsPane = new DataItemsPane(items);
        tabitems.setContent(itemsPane);
        tabPane.getTabs().add(tabitems);


        Tab tabSpells = new Tab("Spells");
        tabSpells.setClosable(false);
        DataSpellPane spellsPane = new DataSpellPane(spells);
        tabSpells.setContent(spellsPane);
        tabPane.getTabs().add(tabSpells);

        return tabPane;

    }

    private void setCurrentProject(Project project) {

        notePane.setCurrentProject(project);

    }


    private void addMenu() {

        final Menu menu1 = new Menu("File");
        final Menu menu2 = new Menu("View");
        final Menu menu3 = new Menu("Help");

        MenuItem newItem = new MenuItem("New project");
        newItem.setOnAction(event ->
        {
            TextInputDialog dialog = new TextInputDialog("new project");
            dialog.setTitle("New project");
            dialog.setHeaderText("Create a new project");
            dialog.setContentText("Please enter a project name:");
            Optional<String> result = dialog.showAndWait();
            result.ifPresent(name -> {
                Project project = noteController.newProject(name);
                Entry entry = new Entry();
                entry.setTitel("Notes");
                entry.setType(NoteTypeEnum.CATEGORY);
                project.setEntry(entry);
                setCurrentProject(project);
            });
        });


        MenuItem openItem = new MenuItem("Open project");
        openItem.setOnAction(event ->
        {
            try {


                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Open Resource File");
                fileChooser.setInitialDirectory(new File(NoteController.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()));
                fileChooser.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("GM tool files", "*.gmt"));

                File selectedFile = fileChooser.showOpenDialog(null);
                if (selectedFile != null) {
                    Project project = Serializer.LoadProject(selectedFile.getAbsolutePath());
                    setCurrentProject(project);
                    System.out.println(project.getTitel());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        MenuItem saveItem = new MenuItem("Save project");
        saveItem.setOnAction(event ->
        {
            if (currentProject != null) {

                noteController.SaveProject(currentProject);
            }
        });

        MenuItem mainMenu = new MenuItem("Main menu");
        mainMenu.setOnAction(event -> border.setCenter(addGridPane()));

        MenuItem characterMenu = new MenuItem("Characters");
        characterMenu.setOnAction(event -> border.setCenter(addCharacterPane()));

        MenuItem notesMenu = new MenuItem("Notes");
        notesMenu.setOnAction(event -> border.setCenter(notePane));

        MenuItem dataMenu = new MenuItem("Data");
        dataMenu.setOnAction(event -> border.setCenter(addDataPane()));

        MenuItem encounterMenu = new MenuItem("Encounters");
        encounterMenu.setOnAction(event -> border.setCenter(new RandomEncounterPane()));



        menuBar.getMenus().addAll(menu1, menu2, menu3);

        menu2.getItems().addAll(mainMenu, characterMenu, notesMenu, dataMenu, encounterMenu);
        menu1.getItems().addAll(newItem, openItem, saveItem);
        border.setTop(menuBar);
    }

    private FlowPane addImageFlowPane() {

        FlowPane flow = new FlowPane();
        flow.setPadding(new Insets(5, 0, 5, 0));
        flow.setVgap(4);
        flow.setHgap(4);
        // flow.setPrefWrapLength(170); // preferred width allows for two columns
        flow.setStyle("-fx-background-color: DAE6F3;");

        Button pages[] = new Button[images.size()];
        for (int i = 0; i < images.size(); i++) {
            final int index = i;
            ImageSelection selection = images.get(i);
            ImageView view = new ImageView(selection.getImage());
            view.setFitHeight(64);
            view.setFitWidth(64);
            pages[i] = new Button("", view);
            pages[i].setTooltip(new Tooltip(selection.getPath()));
            pages[i].setPrefSize(64, 64);
            pages[i].setOnAction(e -> {
                currentImage = images.get(index);
                controller.sendImage(images.get(index));
            });


            flow.getChildren().add(pages[i]);
        }

        return flow;
    }


    public Main getMain() {
        return this;
    }

    @Override
     public void translate(double x, double y) {
        controller.sendTranslate(x, y);
    }

    @Override
    public void zoom(double x, double y) {
        controller.sendZoom(x, y);
    }

    @Override
    public void subtract(Double[] array) {
        controller.sendSubtract(array);
    }
}