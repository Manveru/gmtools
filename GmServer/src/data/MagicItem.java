package data;

import org.apache.commons.csv.CSVRecord;

/**
 * Created by Michel on 29-3-2015.
 */
public class MagicItem extends PathfinderObject
{


    private String Name;
    private String Aura;
    private String CL;
    private String Slot;
    private String Price;
    private String Weight;
    private String Description;
    private String Requirements;
    private String Cost;
    private String Group;
    private String Source;
    private String AL;
    private String Int;
    private String Wis;
    private String Cha;
    private String Ego;
    private String Communication;
    private String Senses;
    private String Powers;
    private String MagicItems;
    private String FullText;
    private String Destruction;
    private String MinorArtifactFlag;
    private String MajorArtifactFlag;
    private String Abjuration;
    private String Conjuration;
    private String Divination;
    private String Enchantment;
    private String Evocation;
    private String Necromancy;
    private String Transmutation;
    private String AuraStrength;
    private String WeightValue;
    private String PriceValue;
    private String CostValue;
    private String Languages;
    private String BaseItem;
    private String LinkText;
    private String Mythic;
    private String LegendaryWeapon;
    private String Illusion;
    private String Universal;

    @Override
    public void parseObject(CSVRecord record) {
        setName(record.get("Name"));
        setAura(record.get("Aura"));
        setCL(record.get("CL"));
        setSlot(record.get("Slot"));
        setPrice(record.get("Price"));
        setWeight(record.get("Weight"));
        setDescription(record.get("Description"));
        setRequirements(record.get("Requirements"));
        setCost(record.get("Cost"));
        setGroup(record.get("Group"));
        setSource(record.get("Source"));
        setAL(record.get("AL"));
        setInt(record.get("Int"));
        setWis(record.get("Wis"));
        setCha(record.get("Cha"));
        setEgo(record.get("Ego"));
        setCommunication(record.get("Communication"));
        setSenses(record.get("Senses"));
        setPowers(record.get("Powers"));
        setMagicItems(record.get("MagicItems"));
        setFullText(record.get("FullText"));
        setDestruction(record.get("Destruction"));
        setMinorArtifactFlag(record.get("MinorArtifactFlag"));
        setMajorArtifactFlag(record.get("MajorArtifactFlag"));
        setAbjuration(record.get("Abjuration"));
        setConjuration(record.get("Conjuration"));
        setDivination(record.get("Divination"));
        setEnchantment(record.get("Enchantment"));
        setEvocation(record.get("Evocation"));
        setNecromancy(record.get("Necromancy"));
        setTransmutation(record.get("Transmutation"));
        setAuraStrength(record.get("AuraStrength"));
        setWeightValue(record.get("WeightValue"));
        setPriceValue(record.get("PriceValue"));
        setCostValue(record.get("CostValue"));
        setLanguages(record.get("Languages"));
        setBaseItem(record.get("BaseItem"));
        setLinkText(record.get("LinkText"));
        setId(Integer.parseInt(record.get("id")));
        setMythic(record.get("Mythic"));
        setLegendaryWeapon(record.get("LegendaryWeapon"));
        setIllusion(record.get("Illusion"));
        setUniversal(record.get("Universal"));

    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAura() {
        return Aura;
    }

    public void setAura(String aura) {
        Aura = aura;
    }

    public String getCL() {
        return CL;
    }

    public void setCL(String CL) {
        this.CL = CL;
    }

    public String getSlot() {
        return Slot;
    }

    public void setSlot(String slot) {
        Slot = slot;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getRequirements() {
        return Requirements;
    }

    public void setRequirements(String requirements) {
        Requirements = requirements;
    }

    public String getCost() {
        return Cost;
    }

    public void setCost(String cost) {
        Cost = cost;
    }

    public String getGroup() {
        return Group;
    }

    public void setGroup(String group) {
        Group = group;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String source) {
        Source = source;
    }

    public String getAL() {
        return AL;
    }

    public void setAL(String AL) {
        this.AL = AL;
    }

    public String getInt() {
        return Int;
    }

    public void setInt(String anInt) {
        Int = anInt;
    }

    public String getWis() {
        return Wis;
    }

    public void setWis(String wis) {
        Wis = wis;
    }

    public String getCha() {
        return Cha;
    }

    public void setCha(String cha) {
        Cha = cha;
    }

    public String getEgo() {
        return Ego;
    }

    public void setEgo(String ego) {
        Ego = ego;
    }

    public String getCommunication() {
        return Communication;
    }

    public void setCommunication(String communication) {
        Communication = communication;
    }

    public String getSenses() {
        return Senses;
    }

    public void setSenses(String senses) {
        Senses = senses;
    }

    public String getPowers() {
        return Powers;
    }

    public void setPowers(String powers) {
        Powers = powers;
    }

    public String getMagicItems() {
        return MagicItems;
    }

    public void setMagicItems(String magicItems) {
        MagicItems = magicItems;
    }

    public String getFullText() {
        return FullText;
    }

    public void setFullText(String fullText) {
        FullText = fullText;
    }

    public String getDestruction() {
        return Destruction;
    }

    public void setDestruction(String destruction) {
        Destruction = destruction;
    }

    public String getMinorArtifactFlag() {
        return MinorArtifactFlag;
    }

    public void setMinorArtifactFlag(String minorArtifactFlag) {
        MinorArtifactFlag = minorArtifactFlag;
    }

    public String getMajorArtifactFlag() {
        return MajorArtifactFlag;
    }

    public void setMajorArtifactFlag(String majorArtifactFlag) {
        MajorArtifactFlag = majorArtifactFlag;
    }

    public String getAbjuration() {
        return Abjuration;
    }

    public void setAbjuration(String abjuration) {
        Abjuration = abjuration;
    }

    public String getConjuration() {
        return Conjuration;
    }

    public void setConjuration(String conjuration) {
        Conjuration = conjuration;
    }

    public String getDivination() {
        return Divination;
    }

    public void setDivination(String divination) {
        Divination = divination;
    }

    public String getEnchantment() {
        return Enchantment;
    }

    public void setEnchantment(String enchantment) {
        Enchantment = enchantment;
    }

    public String getEvocation() {
        return Evocation;
    }

    public void setEvocation(String evocation) {
        Evocation = evocation;
    }

    public String getNecromancy() {
        return Necromancy;
    }

    public void setNecromancy(String necromancy) {
        Necromancy = necromancy;
    }

    public String getTransmutation() {
        return Transmutation;
    }

    public void setTransmutation(String transmutation) {
        Transmutation = transmutation;
    }

    public String getAuraStrength() {
        return AuraStrength;
    }

    public void setAuraStrength(String auraStrength) {
        AuraStrength = auraStrength;
    }

    public String getWeightValue() {
        return WeightValue;
    }

    public void setWeightValue(String weightValue) {
        WeightValue = weightValue;
    }

    public String getPriceValue() {
        return PriceValue;
    }

    public void setPriceValue(String priceValue) {
        PriceValue = priceValue;
    }

    public String getCostValue() {
        return CostValue;
    }

    public void setCostValue(String costValue) {
        CostValue = costValue;
    }

    public String getLanguages() {
        return Languages;
    }

    public void setLanguages(String languages) {
        Languages = languages;
    }

    public String getBaseItem() {
        return BaseItem;
    }

    public void setBaseItem(String baseItem) {
        BaseItem = baseItem;
    }

    public String getLinkText() {
        return LinkText;
    }

    public void setLinkText(String linkText) {
        LinkText = linkText;
    }

    public String getMythic() {
        return Mythic;
    }

    public void setMythic(String mythic) {
        Mythic = mythic;
    }

    public String getLegendaryWeapon() {
        return LegendaryWeapon;
    }

    public void setLegendaryWeapon(String legendaryWeapon) {
        LegendaryWeapon = legendaryWeapon;
    }

    public String getIllusion() {
        return Illusion;
    }

    public void setIllusion(String illusion) {
        Illusion = illusion;
    }

    public String getUniversal() {
        return Universal;
    }

    public void setUniversal(String universal) {
        Universal = universal;
    }

}
