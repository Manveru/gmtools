package data;

import org.apache.commons.csv.CSVRecord;

/**
 * Created by Michel on 29-3-2015.
 */
public class Feat extends PathfinderObject
{
    private String name;
    private String type;
    private String description;
    private String prerequisites;
    private String prerequisite_feats;
    private String benefit;
    private String normal;
    private String special;
    private String source;
    private String fulltext;
    private int teamwork;
    private int critical;
    private int grit;
    private int style;
    private int performance;
    private int racial;
    private int companion_familiar;
    private String race_name;
    private String note;
    private String goal;
    private String completion_benefit;
    private int multiples;
    private String suggested_traits;
    private String prerequisite_skills;
    private int panache;
    private int betrayal;
    private int targeting;

    @Override
    public void parseObject(CSVRecord record) {
        setId(Integer.parseInt(record.get("id")));
        setName(record.get("name"));
        setType(record.get("type"));
        setDescription(record.get("description"));
        setPrerequisites(record.get("prerequisites"));
        setPrerequisite_feats(record.get("prerequisite_feats"));
        setBenefit(record.get("benefit"));
        setNormal(record.get("normal"));
        setSpecial(record.get("special"));
        setSource(record.get("source"));
        setFulltext(record.get("fulltext"));
        setTeamwork(Integer.parseInt(record.get("teamwork")));
        setCritical(Integer.parseInt(record.get("critical")));
        setGrit(Integer.parseInt(record.get("grit")));
        setStyle(Integer.parseInt(record.get("style")));
        setPerformance(Integer.parseInt(record.get("performance")));
        setRacial(Integer.parseInt(record.get("racial")));
        setCompanion_familiar(Integer.parseInt(record.get("companion_familiar")));
        setRace_name(record.get("race_name"));
        setNote(record.get("note"));
        setGoal(record.get("goal"));
        setCompletion_benefit(record.get("completion_benefit"));
        setMultiples(Integer.parseInt(record.get("multiples")));
        setSuggested_traits(record.get("suggested_traits"));
        setPrerequisite_skills(record.get("prerequisite_skills"));
        setPanache(Integer.parseInt(record.get("panache")));
        setBetrayal(Integer.parseInt(record.get("betrayal")));
        setTargeting(Integer.parseInt(record.get("targeting")));

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrerequisites() {
        return prerequisites;
    }

    public void setPrerequisites(String prerequisites) {
        this.prerequisites = prerequisites;
    }

    public String getPrerequisite_feats() {
        return prerequisite_feats;
    }

    public void setPrerequisite_feats(String prerequisite_feats) {
        this.prerequisite_feats = prerequisite_feats;
    }

    public String getBenefit() {
        return benefit;
    }

    public void setBenefit(String benefit) {
        this.benefit = benefit;
    }

    public String getNormal() {
        return normal;
    }

    public void setNormal(String normal) {
        this.normal = normal;
    }

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getFulltext() {
        return fulltext;
    }

    public void setFulltext(String fulltext) {
        this.fulltext = fulltext;
    }

    public int getTeamwork() {
        return teamwork;
    }

    public void setTeamwork(int teamwork) {
        this.teamwork = teamwork;
    }

    public int getCritical() {
        return critical;
    }

    public void setCritical(int critical) {
        this.critical = critical;
    }

    public int getGrit() {
        return grit;
    }

    public void setGrit(int grit) {
        this.grit = grit;
    }

    public int getStyle() {
        return style;
    }

    public void setStyle(int style) {
        this.style = style;
    }

    public int getPerformance() {
        return performance;
    }

    public void setPerformance(int performance) {
        this.performance = performance;
    }

    public int getRacial() {
        return racial;
    }

    public void setRacial(int racial) {
        this.racial = racial;
    }

    public int getCompanion_familiar() {
        return companion_familiar;
    }

    public void setCompanion_familiar(int companion_familiar) {
        this.companion_familiar = companion_familiar;
    }

    public String getRace_name() {
        return race_name;
    }

    public void setRace_name(String race_name) {
        this.race_name = race_name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public String getCompletion_benefit() {
        return completion_benefit;
    }

    public void setCompletion_benefit(String completion_benefit) {
        this.completion_benefit = completion_benefit;
    }

    public int getMultiples() {
        return multiples;
    }

    public void setMultiples(int multiples) {
        this.multiples = multiples;
    }

    public String getSuggested_traits() {
        return suggested_traits;
    }

    public void setSuggested_traits(String suggested_traits) {
        this.suggested_traits = suggested_traits;
    }

    public String getPrerequisite_skills() {
        return prerequisite_skills;
    }

    public void setPrerequisite_skills(String prerequisite_skills) {
        this.prerequisite_skills = prerequisite_skills;
    }

    public int getPanache() {
        return panache;
    }

    public void setPanache(int panache) {
        this.panache = panache;
    }

    public int getBetrayal() {
        return betrayal;
    }

    public void setBetrayal(int betrayal) {
        this.betrayal = betrayal;
    }

    public int getTargeting() {
        return targeting;
    }

    public void setTargeting(int targeting) {
        this.targeting = targeting;
    }



}
