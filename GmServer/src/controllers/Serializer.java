package controllers;

import data.notes.Project;
import org.apache.commons.codec.binary.Base64;

import java.io.*;
import java.util.Scanner;

public class Serializer {
    public static void SaveProject(String path, String name, Project project) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);

            oos.writeObject(project);
            oos.flush();
            byte[] binary = baos.toByteArray();
            String text = Base64.encodeBase64String(binary);
            //System.out.println(text);
            System.out.println(path + "\\" + name + ".gmt");
            PrintWriter out = new PrintWriter(path + "\\" + name + ".gmt");
            out.println(text);
            out.flush();
            out.close();


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public static Project LoadProject(String file) {
        try {
            String text = readFile(file);

            byte[] binary = Base64.decodeBase64(text);

            ByteArrayInputStream baos = new ByteArrayInputStream(binary);
            ObjectInputStream oos = new ObjectInputStream(baos);

            Project object = (Project) oos.readObject();

            oos.close();
            baos.close();
            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String readFile(String path) throws IOException {

        File file = new File(path);
        try (Scanner scanner = new Scanner(file)) {
            return scanner.next();
        }
    }

}
