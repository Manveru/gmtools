package controllers;

import data.PcgFile;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class PcGenController
{
    public ArrayList<PcgFile> getTabData(String path) {
        ArrayList<PcgFile> pcgFileArrayList = new ArrayList<>();
        File file = new File(path);
        String[] directories = file.list((current, name) -> new File(current, name).isDirectory());
        if (directories != null) {
            for (String directoryName : directories) {
                File directory = new File(path + "\\" + directoryName);
                String[] pcrFiles = directory.list((current, name) -> name.endsWith(".pcg"));

                if (pcrFiles != null && pcrFiles.length > 0) {
                    pcgFileArrayList.add(getPcrFileData(path + "\\" + directoryName + "\\" + pcrFiles[0]));
                }
            }
        }

        return pcgFileArrayList;
    }


    public PcgFile getPcrFileData(String path)
    {
        try {
            String file = readFile(path, Charset.defaultCharset());
            System.out.println(file);
            String[] lines = file.split("\\r?\\n");
            PcgFile pcgFile = new PcgFile(file);

            for (String line : lines) {
                String[] keyValue = line.split(":");
                if (keyValue.length == 2) {
                    switch (keyValue[0]) {
                        case "CHARACTERNAME":
                            pcgFile.setName(keyValue[1]);
                        case "CHARACTERBIO":
                            pcgFile.setBio(keyValue[1]);
                        case "CHARACTERDESC":
                            pcgFile.setDescription(keyValue[1]);
                    }
                }
            }

            return pcgFile;
        }
        catch (Exception ex)
        {
            return new PcgFile(ex.getMessage());
        }

    }

    static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

}
